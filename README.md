# Backtester

## Report
The report can be found [here](REPORT.md).

## Git
```
git checkout master
git fetch origin && git pull
git checkout -b <initials>-<identifier>
git rebase master -i
git push origin --set-upstream <initials>-<identifier>
```
Notes
- You will need to branch out, nobody has permissions to push to master
- Name your branch with your initials (e.g. NF) and a very short human readable identifier
- Work small, once something works (even if it doesn't actually do anything), merge to main
- With everyone working on different files, conflicts should not really happen. However, always rebase your branch onto the most current master before pushing and merging. If something goes wack, ask Nairen.

For merge requests and commits, please prefix the entry with `[milestone-task]`, e.g. `[data-001] Implemented xxx.`, where
- `milestone` is one of `data, ob, ome, strat`,
- `task` is 0-padded, 3 digit global task number,
- Commit messages should be succinct and start with an action.
Branch-internal commit messages are up to your workflow.

## Dependencies
- CMake >= 3.22
- G++ >= 8

## Running
```
$ vagrant up backtester
$ vagrant ssh backtester -c backtester
$ vagrant halt
```
After making changes, to rebuild, call
```
$ vagrant provision --provision-with compile
$ vagrant ssh backtester -c backtester
```
To run web app,
```
$ vagrant ssh backtester -c "DB_PATH=/vagrant/data/dev.db node web/dev.js"
```
To test,
```
$ vagrant ssh backtester -c "tests --gtest_color=yes"
```

## People
- Nairen Fu nairenf2@illinois.edu
- Bohan Liu bohan3@illinois.edu 
- Pavan Otthi potthi2@illinois.edu
