from csv import reader

if __name__ == '__main__':
    trades = 0
    with open('./data/07302019.NASDAQ_ITCH50_AAPL_message.csv', 'r') as f:
        csv_reader = reader(f)

        for row in csv_reader:
            if row[1] in ('C', 'E'):
                trades += 1

    print(f'Trades: {trades}')
