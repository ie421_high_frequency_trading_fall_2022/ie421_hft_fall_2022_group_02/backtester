#include <gtest/gtest.h>
#include "TimeManager.h"
#include <memory>
#include <thread>
#include <chrono>


static int x;
void fetch(std::shared_ptr<TimeManager> p) {
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    p->ProceedFetchToTime(10);
    x = 0;
}

void run(std::shared_ptr<TimeManager> p) {
    p->ProceedSnapshot();
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    x = 1;
}


TEST(TimeManager, All) {
    std::shared_ptr<TimeManager> p = std::make_shared<TimeManager>(1, 1);
    std::thread t1(fetch, p), t2(run, p);
    t2.join(); t1.join();

    EXPECT_EQ(x, 1);
    
}

