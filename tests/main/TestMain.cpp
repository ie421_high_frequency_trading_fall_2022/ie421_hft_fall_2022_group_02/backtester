#include "MockExchange.h"

#include "MockStrategy.hpp" 
#include "MockDataLoader.hpp"
#include "Backtester.h"

#include <gtest/gtest.h>

TEST(Main, All) {
    auto ex = std::make_shared<MockExchange>();
    auto st = std::make_shared<MockStrategy>();
    auto tm = std::make_shared<TimeManager>(0,-1);
    Backtester bk = Backtester(tm);
    
    bk.registerExchange(ex);
    bk.registerStrategy(st);

    MockDataLoader dl = MockDataLoader();
    dl.registerExchange(st);
    dl.registerTimeManager(tm);

    EXPECT_CALL(*ex, onEventD()).Times(3);
    EXPECT_CALL(*st, onEventA()).Times(3);

    std::thread t1([&] () -> void {bk.run();}), t2([&] (auto k) -> void {k.run();}, dl);

    t1.join(); t2.join();

    
}
