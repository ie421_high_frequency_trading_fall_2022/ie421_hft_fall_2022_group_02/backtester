#include <gtest/gtest.h>
#include <StrategyInterface.h>

#define ADD(event) static_pointer_cast<AddMessage>(event)
#define CANCEL(event) static_pointer_cast<CancelMessage>(event)
#define UPDATE(event) static_pointer_cast<UpdateMessage>(event)
#define TRADE(event) static_pointer_cast<TradeMessage>(event)
// TEST(StrategyTest, strategyonevent)
// {
//     StrategyInterface strat;

//     TradeMessage h0;
//     h0.deliver_time = 0;
//     h0.source = SOURCE_STRATEGY;
//     h0.matchNumber = 0;
//     h0.quantity = 1000;
//     h0.price = 10.00;

//     strat.onEvent(std::make_shared<TradeMessage>(h0));

//     EXPECT_EQ(strat.Trades[0].deliver_time, 0);
//     EXPECT_EQ(strat.Trades[0].source, SOURCE_STRATEGY);
//     EXPECT_EQ(strat.Trades[0].matchNumber, 0);
//     EXPECT_EQ(strat.Trades[0].quantity, 1000);
//     EXPECT_EQ(strat.Trades[0].price, 10.00);
// }

// TEST(StrategyTest, SetInitialAmmount)
// {
//     StrategyInterface strat;

//     strat.setInitialAmount(20000);

//     EXPECT_EQ(strat.initialAmount, 20000);
// }

// TEST(StrategyTest, orderbookBids)
// {
//     StrategyInterface strat;

//     strat.setInitialAmount(20000);

//     strat.placeBid(20.0, 100);

//     strat.onEvent(strat.get());

//     EXPECT_EQ(strat.orderbookBids.size(), 1);
//     EXPECT_EQ(strat.orderbookBids[0].price, 20);
// }

// TEST(StrategyTest, orderbookAsks)
// {
//     StrategyInterface strat;

//     strat.setInitialAmount(20000);

//     strat.placeAsk(20.0, 100);

//     strat.onEvent(strat.get());

//     EXPECT_EQ(strat.orderbookAsks.size(), 1);
//     EXPECT_EQ(strat.orderbookAsks[0].price, 20);
// }

// TEST(StrategyTest, onTrade)
// {
//     StrategyInterface strat;

//     strat.setInitialAmount(20000);

//     TradeMessage h0;
//     h0.deliver_time = 0;
//     h0.source = SOURCE_STRATEGY;
//     h0.matchNumber = 0;
//     h0.quantity = 1000;
//     h0.price = 10.00;

//     strat.onEvent(std::make_shared<TradeMessage>(h0));

//     EXPECT_EQ(strat.CurrentShares, 1000);
//     EXPECT_EQ(strat.currentAmount, 10000);
// }

// TEST(StrategyTest, placeOrder)
// {

//     StrategyInterface strat;

//     strat.setInitialAmount(20000);

//     strat.placeAsk(20.0, 100);

//     strat.onEvent(strat.get());

//     // strat.cancelOrder(-2, 100);

//     // EXPECT_EQ()
//     EXPECT_EQ(UPDATE(strat.get())->orderNumber, -2);
// }

// TEST(StrategyTest, updateOrder)
// {
//     StrategyInterface strat;

//     strat.setInitialAmount(20000);

//     strat.placeAsk(20.0, 100);

//     strat.onEvent(strat.get());
// }

// TEST(StrategyTest, cancelOrder)
// {

// }

// TEST(StrategyTest, placeAsk)
// {
//     StrategyInterface strat;

//     strat.setInitialAmount(20000);

//     strat.placeAsk(20.0, 100);

//     strat.onEvent(strat.get());

//     EXPECT_EQ(strat.orderbookAsks.size(), 1);
//     EXPECT_EQ(strat.orderbookAsks[0].price, 20);
// }

// TEST(StrategyTest, placeBid)
// {
//     StrategyInterface strat;

//     strat.setInitialAmount(20000);

//     strat.placeBid(20.0, 100);

//     strat.onEvent(strat.get());

//     EXPECT_EQ(strat.orderbookBids.size(), 1);
//     EXPECT_EQ(strat.orderbookBids[0].price, 20);
// }

// TEST(StrategyTest, getPlacedOrders)
// {

//     StrategyInterface strat;

//     strat.setInitialAmount(20000);

//     strat.placeBid(20.0, 100);

//     strat.onEvent(strat.get());

//     EXPECT_EQ(strat.PlacedOrders[-2], 1);
//     EXPECT_EQ(strat.orderbookBids.size(), 1);
//     EXPECT_EQ(strat.orderbookBids[0].price, 20);
// }

// TEST(StrategyTest, getTrades)
// {
//     StrategyInterface strat;

//     strat.setInitialAmount(20000);

//     TradeMessage h0;
//     h0.deliver_time = 0;
//     h0.source = SOURCE_STRATEGY;
//     h0.matchNumber = 0;
//     h0.quantity = 1000;
//     h0.price = 10.00;

//     strat.onEvent(std::make_shared<TradeMessage>(h0));

//     EXPECT_EQ(strat.getTrades()[0].price, 10.00);
//     EXPECT_EQ(strat.CurrentShares, 1000);
//     EXPECT_EQ(strat.currentAmount, 10000);
// }

// TEST(StrategyTest, getNetProfit)
// {

//     StrategyInterface strat;
//     strat.setInitialAmount(20000);

//     TradeMessage h0;
//     h0.deliver_time = 0;
//     h0.source = SOURCE_STRATEGY;
//     h0.matchNumber = 0;
//     h0.quantity = 1000;
//     h0.price = 10.00;

//     strat.onEvent(std::make_shared<TradeMessage>(h0));
//     EXPECT_EQ(strat.initialAmount, 20000);
//     EXPECT_EQ(strat.currentAmount, 10000);
//     EXPECT_EQ(strat.getNetProfit(), -10000);
// }

// TEST(StrategyTest, BalanceHistory)
// {

//     StrategyInterface strat;
//     strat.setInitialAmount(20000);

//     TradeMessage h0;
//     h0.deliver_time = 0;
//     h0.source = SOURCE_STRATEGY;
//     h0.matchNumber = 0;
//     h0.quantity = 1000;
//     h0.price = 10.00;
//     h0.aggressorOrderNumber = -2;
//     h0.orderNumber = 13;
//     h0.aggressedSide = Side::S;

//     TradeMessage h1;
//     h1.deliver_time = 10;
//     h1.source = SOURCE_STRATEGY;
//     h1.matchNumber = 2;
//     h1.quantity = 500;
//     h1.price = 5.00;
//     h1.aggressorOrderNumber = -4;
//     h1.orderNumber = 13;
//     h1.aggressedSide = Side::B;

//     strat.onEvent(std::make_shared<TradeMessage>(h0));

//     EXPECT_EQ(strat.currentAmount, 10000);

//     strat.onEvent(std::make_shared<TradeMessage>(h1));

//     EXPECT_EQ(strat.initialAmount, 20000);
//     EXPECT_EQ(strat.currentAmount, 12500);
//     EXPECT_EQ(strat.getNetProfit(), -7500);
//     EXPECT_EQ(strat.BalanceHistory[0], 20000);
// }

// TEST(StrategyTest, setReturnsFrequency)
// {
//     StrategyInterface strat;
//     strat.setInitialAmount(20000);

//     strat.SetReturnsFrequency(500);

//     EXPECT_EQ(strat.ReturnsFrequency, 500);
// }

TEST(StrategyTest, getReturns)
{

    StrategyInterface strat;
    strat.setInitialAmount(20000);

    strat.SetReturnsFrequency(100);

    TradeMessage h0;
    h0.deliver_time = 0;
    h0.source = SOURCE_STRATEGY;
    h0.matchNumber = 0;
    h0.quantity = 1000;
    h0.price = 10.00;
    h0.aggressorOrderNumber = -2;
    h0.orderNumber = 2;
    h0.aggressedSide = Side::S;

    TradeMessage h1;
    h1.deliver_time = 10;
    h1.source = SOURCE_HISTORICAL;
    h1.matchNumber = 1;
    h1.quantity = 500;
    h1.price = 5.00;
    h1.aggressorOrderNumber = 3;
    h1.orderNumber = -3;
    h1.aggressedSide = Side::S;

    TradeMessage h2;
    h2.deliver_time = 50;
    h2.source = SOURCE_HISTORICAL;
    h2.matchNumber = 2;
    h2.quantity = 200;
    h2.price = 10.00;
    h2.aggressorOrderNumber = 4;
    h2.orderNumber = -4;
    h2.aggressedSide = Side::S;

    TradeMessage h3;
    h3.deliver_time = 100;
    h3.source = SOURCE_STRATEGY;
    h3.matchNumber = 3;
    h3.quantity = 400;
    h3.price = 5.00;
    h3.aggressorOrderNumber = -5;
    h3.orderNumber = 5;
    h3.aggressedSide = Side::S;

    TradeMessage h4;
    h4.deliver_time = 400;
    h4.source = SOURCE_HISTORICAL;
    h4.matchNumber = 4;
    h4.quantity = 100;
    h4.price = 10.00;
    h4.aggressorOrderNumber = 6;
    h4.orderNumber = -6;
    h4.aggressedSide = Side::B;

    TradeMessage h5;
    h5.deliver_time = 600;
    h5.source = SOURCE_STRATEGY;
    h5.matchNumber = 5;
    h5.quantity = 400;
    h5.price = 5.00;
    h5.aggressorOrderNumber = -7;
    h5.orderNumber = 7;
    h5.aggressedSide = Side::S;

    strat.onEvent(std::make_shared<TradeMessage>(h0));
    strat.onEvent(std::make_shared<TradeMessage>(h1));
    strat.onEvent(std::make_shared<TradeMessage>(h2));
    strat.onEvent(std::make_shared<TradeMessage>(h3));
    strat.onEvent(std::make_shared<TradeMessage>(h4));
    strat.onEvent(std::make_shared<TradeMessage>(h5));

    strat.getReturns();
    EXPECT_EQ(strat.BalanceHistory[0], 20000);
    EXPECT_EQ(strat.BalanceHistory[10], 15000);
    EXPECT_EQ(strat.BalanceHistory[50], 17500);
    EXPECT_EQ(strat.BalanceHistory[100], 16000);
    EXPECT_EQ(strat.BalanceHistory[400], 19500);
    EXPECT_EQ(strat.BalanceHistory[600], 15500);
    EXPECT_EQ(strat.Returns.size(), 3);
    EXPECT_EQ(strat.Returns[0], -.2);
    EXPECT_EQ(strat.Returns[1], .21875);
}