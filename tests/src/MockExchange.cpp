#include "MockExchange.h"

void MockExchange::onEvent(std::shared_ptr<Event> evt) {
    switch (evt->type) {
        case 'D':
            onEventD();
            Event a;
            a.deliver_time = evt->deliver_time + 10;
            a.type = 'A';
            _pending_events.push(std::make_shared<Event>(a));
        
        default :
            return;
    }
}

std::shared_ptr<Event> MockExchange::get() {
    this->onEvent(_pending_events.top());
    return _pending_events.top();
}

EventQueue& MockExchange::getEvents() {
    return _pending_events;
}

