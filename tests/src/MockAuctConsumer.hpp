#pragma once
#include "IAuctConsumer.h"

#include <gmock/gmock.h>

class MockAuctConsumer : public IAuctConsumer {
public:
  MockAuctConsumer(){};
  ~MockAuctConsumer(){};

  MOCK_METHOD(void, onAdd, (const AddMessage& msg), (override));
  MOCK_METHOD(void, onUpdate, (const UpdateMessage& msg), (override));
  MOCK_METHOD(void, onCancel, (const CancelMessage& msg), (override));
  MOCK_METHOD(void, onTrade, (const TradeMessage& msg), (override));
};

