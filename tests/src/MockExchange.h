#pragma once

#include <gmock/gmock.h>
#include "EventListener.h"

class MockExchange : public EventListener
{
private:
    /* data */
public:
    void onEvent(std::shared_ptr<Event> evt) override;
    std::shared_ptr<Event> get() override;
    EventQueue& getEvents();
    MOCK_METHOD(void, onEventA, (), ());
    MOCK_METHOD(void, onEventD, (), ());
};

