#pragma once
#include <memory> 
#include <chrono>
#include <iostream>
#include <thread>
#include "MockExchange.h"
#include "TimeManager.h"


class MockDataLoader {
    private:
        Event evts[3] = {
            {'D', 3, 3, nullptr}, 
            {'D', 4, 4, nullptr}, 
            {'D', 5, 10, nullptr}, 
        };
        std::shared_ptr<EventListener> exchange;
        std::shared_ptr<TimeManager> tmgr;
    public:
        void registerExchange(std::shared_ptr<EventListener> ex);
        void registerTimeManager(std::shared_ptr<TimeManager> tmgr);
        void run();
};


void MockDataLoader::registerExchange(std::shared_ptr<EventListener> ex) {
    this->exchange = ex;
}

void MockDataLoader::registerTimeManager(std::shared_ptr<TimeManager> tmgr) {
    this->tmgr = tmgr;
}

void MockDataLoader::run() {
    for (int i = 0; i < 3; i++) {
        exchange->push(std::make_shared<Event>(evts[i]));
        tmgr->ProceedFetchToTime(evts[i].deliver_time);
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
        
    }
    tmgr->session_ended = true;
    tmgr->ProceedFetchToTime(LONG_MAX);
    std::cout << "DL complete!" << std::endl;
}
