#pragma once

#include <gmock/gmock.h>
#include "EventListener.h"

class MockStrategy : public EventListener
{
private:
    /* data */
public:
    MockStrategy() {};
    ~MockStrategy() {};
    
    void onEvent(std::shared_ptr<Event> evt) override;
    MOCK_METHOD(void, onEventA, (), ());
    
};

void MockStrategy::onEvent(std::shared_ptr<Event> evt) {
    switch (evt->type) {
        case 'A':
            onEventA();
            
        default :
            return;
    }
}
