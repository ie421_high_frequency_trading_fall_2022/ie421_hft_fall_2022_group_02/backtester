#include <gtest/gtest.h>
#include "Exchange.h"

TEST(ExchangeTest, Events) {
  Exchange exchange;

  AddMessage msg0;
  msg0.deliver_time = 0;
  msg0.orderNumber = 111;
  msg0.side = Side::B;
  msg0.quantity = 1000;
  msg0.price = 10.00;
  msg0.source = SOURCE_HISTORICAL;

  exchange.onEvent(std::make_shared<AddMessage>(msg0));

  std::shared_ptr<Event> event = exchange.get();
  EXPECT_EQ(*static_pointer_cast<AddMessage>(event), msg0);
}

