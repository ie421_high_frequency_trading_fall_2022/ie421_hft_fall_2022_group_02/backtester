#include <gtest/gtest.h>
#include <memory>
#include "messages.h"

TEST(MessagesTest, TestConversion) {
  std::shared_ptr<AddMessage> msg = std::make_shared<AddMessage>();
  EXPECT_EQ(msg->type, Type::A);
  msg->stockLocate    = 0;
  msg->orderNumber    = 1;
  msg->sequenceNumber = 2;
  msg->timestamp      = 3;
  msg->source         = SOURCE_STRATEGY;
  msg->side           = Side::S;
  msg->quantity       = 4;
  msg->price          = 5;

  AddMessage exp = *msg;
  std::shared_ptr<AuctMessage> base = std::move(msg);
  std::shared_ptr<AddMessage> derived = static_pointer_cast<AddMessage>(base);

  EXPECT_EQ(derived->stockLocate, 0);
  EXPECT_EQ(derived->orderNumber, 1);
  EXPECT_EQ(derived->sequenceNumber, 2);
  EXPECT_EQ(derived->timestamp, 3);
  EXPECT_EQ(derived->source, SOURCE_STRATEGY);
  EXPECT_EQ(derived->side, Side::S);
  EXPECT_EQ(derived->quantity, 4);
  EXPECT_EQ(derived->price, 5);
}
