#include <gtest/gtest.h>
#include "MatchingEngine.h"
#include "MockExchange.h"

#define AUCT(event) static_pointer_cast<AuctMessage>(event)
#define TRADE(event) static_pointer_cast<TradeMessage>(event)

void printEvents(EventQueue& events) {
  while (!events.empty()) {
    std::shared_ptr<AuctMessage> msg = static_pointer_cast<AuctMessage>(AUCT(events.top()));
    printf("TX: %ld, TP: %d, ON: %ld\n", msg->deliver_time, msg->type, msg->orderNumber);
    events.pop();
  }
}

TEST(MatchingEngineTest, OnAddStrategyNoMatch) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  MatchingEngine engine(&listener);

  AddMessage h0;
  h0.deliver_time = 0;
  h0.deliver_time = h0.deliver_time;
  h0.source = SOURCE_HISTORICAL;
  h0.orderNumber = 0;
  h0.side = Side::S;
  h0.quantity = 1000;
  h0.price = 10.00;

  AddMessage s0;
  s0.deliver_time = 1;
  s0.source = SOURCE_STRATEGY;
  s0.orderNumber = 100;
  s0.side = Side::B;
  s0.quantity = h0.quantity;
  s0.price = h0.price - 1.00;

  Order exp0(h0.orderNumber, h0.side, h0.quantity, h0.price);
  Order exp1(s0.orderNumber, s0.side, s0.quantity, s0.price);

  engine.onAdd(h0);
  engine.onAdd(s0);

  EXPECT_EQ(*engine.snapshot().asks().at(h0.price).front(), exp0);
  EXPECT_EQ(*engine.snapshot().bids().at(s0.price).front(), exp1);

  EXPECT_EQ(events.size(), 2);
  EXPECT_EQ(AUCT(events.top())->orderNumber, 0);
  EXPECT_EQ(AUCT(events.top())->type, Type::A);
  events.pop();
  EXPECT_EQ(AUCT(events.top())->orderNumber, 100);
  EXPECT_EQ(AUCT(events.top())->type, Type::A);
}

TEST(MatchingEngineTest, OnAddStrategyPerfectMatch) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  MatchingEngine engine(&listener);

  AddMessage h0;
  h0.deliver_time = 0;
  h0.deliver_time = h0.deliver_time;
  h0.source = SOURCE_HISTORICAL;
  h0.orderNumber = 0;
  h0.side = Side::S;
  h0.quantity = 1000;
  h0.price = 10.00;

  AddMessage s0;
  s0.deliver_time = 1;
  s0.deliver_time = s0.deliver_time;
  s0.source = SOURCE_STRATEGY;
  s0.orderNumber = 100;
  s0.side = Side::B;
  s0.quantity = h0.quantity;
  s0.price = h0.price;

  engine.onAdd(h0);
  engine.onAdd(s0);

  EXPECT_FALSE(engine.snapshot().asks().contains(h0.price));

  EXPECT_EQ(events.size(), 3);
  EXPECT_EQ(AUCT(events.top())->orderNumber, 0);
  EXPECT_EQ(AUCT(events.top())->type, Type::A);
  events.pop();
  EXPECT_EQ(AUCT(events.top())->orderNumber, 0);
  EXPECT_EQ(AUCT(events.top())->type, Type::T);
  EXPECT_EQ(TRADE(events.top())->aggressorOrderNumber, s0.orderNumber);
  events.pop();
  EXPECT_EQ(AUCT(events.top())->orderNumber, 0);
  EXPECT_EQ(AUCT(events.top())->type, Type::C);
  // TODO need to think about how to reply to strategy that their trade got executed
}

TEST(MatchingEngineTest, OnAddStrategyMatchMultipleHistorical) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  MatchingEngine engine(&listener);

  AddMessage h0;
  h0.deliver_time = 0;
  h0.deliver_time = h0.deliver_time;
  h0.source = SOURCE_HISTORICAL;
  h0.orderNumber = 0;
  h0.side = Side::S;
  h0.quantity = 1000;
  h0.price = 10.00;

  AddMessage h1;
  h1.deliver_time = 1;
  h1.deliver_time = h1.deliver_time;
  h1.source = SOURCE_HISTORICAL;
  h1.orderNumber = h0.orderNumber + 1;
  h1.side = Side::S;
  h1.quantity = 1000;
  h1.price = h0.price;

  AddMessage h2;
  h2.deliver_time = 2;
  h2.deliver_time = h2.deliver_time;
  h2.source = SOURCE_HISTORICAL;
  h2.orderNumber = h1.orderNumber + 1;
  h2.side = Side::S;
  h2.quantity = 1000;
  h2.price = h0.price;

  AddMessage s0;
  s0.deliver_time = 3;
  s0.deliver_time = s0.deliver_time;
  s0.source = SOURCE_STRATEGY;
  s0.orderNumber = 100;
  s0.side = Side::B;
  s0.quantity = h0.quantity + h1.quantity + h2.quantity / 2;
  s0.price = h0.price;

  Order exp2(h2.orderNumber, h2.side, h2.quantity / 2, h2.price);

  engine.onAdd(h0);
  engine.onAdd(h1);
  engine.onAdd(h2);
  engine.onAdd(s0);

  EXPECT_EQ(*engine.snapshot().asks().at(h0.price).front(), exp2);

  // TODO make the order more deterministic
  EXPECT_EQ(events.size(), 9);
  EXPECT_EQ(AUCT(events.top())->orderNumber, 0);
  EXPECT_EQ(AUCT(events.top())->type, Type::A);
  events.pop();
  EXPECT_EQ(AUCT(events.top())->orderNumber, 1);
  EXPECT_EQ(AUCT(events.top())->type, Type::A);
  events.pop();
  EXPECT_EQ(AUCT(events.top())->orderNumber, 2);
  EXPECT_EQ(AUCT(events.top())->type, Type::A);
  events.pop();
  EXPECT_EQ(AUCT(events.top())->orderNumber, 1);
  EXPECT_EQ(AUCT(events.top())->type, Type::C);
  events.pop();
  EXPECT_EQ(AUCT(events.top())->orderNumber, 1);
  EXPECT_EQ(AUCT(events.top())->type, Type::T);
  events.pop();
  EXPECT_EQ(AUCT(events.top())->orderNumber, 2);
  EXPECT_EQ(AUCT(events.top())->type, Type::T);
  events.pop();
  EXPECT_EQ(AUCT(events.top())->orderNumber, 2);
  EXPECT_EQ(AUCT(events.top())->type, Type::C);
  events.pop();
  EXPECT_EQ(AUCT(events.top())->orderNumber, 0);
  EXPECT_EQ(AUCT(events.top())->type, Type::C);
  events.pop();
  EXPECT_EQ(AUCT(events.top())->orderNumber, 0);
  EXPECT_EQ(AUCT(events.top())->type, Type::T);
  events.pop();
}

TEST(MatchingEngineTest, OnAddStrategyMatchPortionHistorical) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  MatchingEngine engine(&listener);

  AddMessage h0;
  h0.deliver_time = 0;
  h0.deliver_time = h0.deliver_time;
  h0.source = SOURCE_HISTORICAL;
  h0.orderNumber = 0;
  h0.side = Side::S;
  h0.quantity = 1000;
  h0.price = 10.00;

  AddMessage s0;
  s0.deliver_time = 1;
  s0.deliver_time = s0.deliver_time;
  s0.source = SOURCE_STRATEGY;
  s0.orderNumber = 100;
  s0.side = Side::B;
  s0.quantity = h0.quantity / 2;
  s0.price = h0.price;

  Order exp0(h0.orderNumber, h0.side, h0.quantity / 2, h0.price);

  engine.onAdd(h0);
  engine.onAdd(s0);

  EXPECT_EQ(*engine.snapshot().asks().at(h0.price).front(), exp0);

  EXPECT_EQ(events.size(), 3);
  EXPECT_EQ(AUCT(events.top())->orderNumber, 0);
  EXPECT_EQ(AUCT(events.top())->type, Type::A);
  events.pop();
  EXPECT_EQ(AUCT(events.top())->orderNumber, 0);
  EXPECT_EQ(AUCT(events.top())->type, Type::T);
  events.pop();
  EXPECT_EQ(AUCT(events.top())->orderNumber, 0);
  EXPECT_EQ(AUCT(events.top())->type, Type::C);
  events.pop();
}

TEST(MatchingEngineTest, OnAddStrategyBidMatchLowerAsk) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  MatchingEngine engine(&listener);

  AddMessage h0;
  h0.deliver_time = 0;
  h0.deliver_time = h0.deliver_time;
  h0.source = SOURCE_HISTORICAL;
  h0.orderNumber = 0;
  h0.side = Side::S;
  h0.quantity = 1000;
  h0.price = 10.00;

  AddMessage h1;
  h1.deliver_time = 1;
  h1.deliver_time = h1.deliver_time;
  h1.source = SOURCE_HISTORICAL;
  h1.orderNumber = h0.orderNumber + 1;
  h1.side = Side::S;
  h1.quantity = h0.quantity;
  h1.price = 20.00;

  AddMessage s0;
  s0.deliver_time = 2;
  s0.deliver_time = s0.deliver_time;
  s0.source = SOURCE_STRATEGY;
  s0.orderNumber = 100;
  s0.side = Side::B;
  s0.quantity = h0.quantity * 2;
  s0.price = 15.00;

  Order exp1(h1.orderNumber, h1.side, h1.quantity, h1.price);
  Order exp2(s0.orderNumber, s0.side, s0.quantity - h0.quantity, s0.price);

  engine.onAdd(h0);
  engine.onAdd(h1);
  engine.onAdd(s0);

  EXPECT_EQ(*engine.snapshot().asks().at(h1.price).front(), exp1);
  EXPECT_EQ(*engine.snapshot().bids().at(s0.price).front(), exp2);

  EXPECT_EQ(events.size(), 5);
  EXPECT_EQ(AUCT(events.top())->orderNumber, 0);
  EXPECT_EQ(AUCT(events.top())->type, Type::A);
  events.pop();
  EXPECT_EQ(AUCT(events.top())->orderNumber, 1);
  EXPECT_EQ(AUCT(events.top())->type, Type::A);
  events.pop();
  EXPECT_EQ(AUCT(events.top())->orderNumber, 0);
  EXPECT_EQ(AUCT(events.top())->type, Type::T);
  events.pop();
  // TODO remove the cancels
  EXPECT_EQ(AUCT(events.top())->orderNumber, 0);
  EXPECT_EQ(AUCT(events.top())->type, Type::C);
  events.pop();
  EXPECT_EQ(AUCT(events.top())->orderNumber, 100);
  EXPECT_EQ(AUCT(events.top())->type, Type::A);
  events.pop();
}

TEST(MatchingEngineTest, OnAddStrategyAskMatchHigherBid) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  MatchingEngine engine(&listener);

  AddMessage h0;
  h0.deliver_time = 0;
  h0.deliver_time = h0.deliver_time;
  h0.source = SOURCE_HISTORICAL;
  h0.orderNumber = 0;
  h0.side = Side::B;
  h0.quantity = 1000;
  h0.price = 10.00;

  AddMessage h1;
  h1.deliver_time = 1;
  h1.deliver_time = h1.deliver_time;
  h1.source = SOURCE_HISTORICAL;
  h1.orderNumber = h0.orderNumber + 1;
  h1.side = Side::B;
  h1.quantity = h0.quantity;
  h1.price = 20.00;

  AddMessage s0;
  s0.deliver_time = 2;
  s0.deliver_time = s0.deliver_time;
  s0.source = SOURCE_STRATEGY;
  s0.orderNumber = 100;
  s0.side = Side::S;
  s0.quantity = h0.quantity * 2;
  s0.price = 15.00;

  Order exp0(h0.orderNumber, h0.side, h0.quantity, h0.price);
  Order exp2(s0.orderNumber, s0.side, s0.quantity - h0.quantity, s0.price);

  engine.onAdd(h0);
  engine.onAdd(h1);
  engine.onAdd(s0);

  EXPECT_EQ(*engine.snapshot().bids().at(h0.price).front(), exp0);
  EXPECT_EQ(*engine.snapshot().asks().at(s0.price).front(), exp2);

  EXPECT_EQ(events.size(), 5);
  EXPECT_EQ(AUCT(events.top())->orderNumber, 0);
  EXPECT_EQ(AUCT(events.top())->type, Type::A);
  events.pop();
  EXPECT_EQ(AUCT(events.top())->orderNumber, 1);
  EXPECT_EQ(AUCT(events.top())->type, Type::A);
  events.pop();
  EXPECT_EQ(AUCT(events.top())->orderNumber, 1);
  EXPECT_EQ(AUCT(events.top())->type, Type::T);
  events.pop();
  EXPECT_EQ(AUCT(events.top())->orderNumber, 1);
  EXPECT_EQ(AUCT(events.top())->type, Type::C);
  events.pop();
  EXPECT_EQ(AUCT(events.top())->orderNumber, 100);
  EXPECT_EQ(AUCT(events.top())->type, Type::A);
  events.pop();
}

TEST(MatchingEngineTest, OnAddStrategyMatchMultipleLevels) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  MatchingEngine engine(&listener);

  AddMessage h0;
  h0.source = SOURCE_HISTORICAL;
  h0.orderNumber = 0;
  h0.side = Side::S;
  h0.quantity = 1000;
  h0.price = 10.00;

  AddMessage h1;
  h1.source = SOURCE_HISTORICAL;
  h1.orderNumber = h0.orderNumber + 1;
  h1.side = Side::S;
  h1.quantity = h0.quantity;
  h1.price = 12.00;

  AddMessage s0;
  s0.source = SOURCE_STRATEGY;
  s0.orderNumber = 100;
  s0.side = Side::B;
  s0.quantity = h0.quantity + h1.quantity;
  s0.price = 15.00;

  engine.onAdd(h0);
  engine.onAdd(h1);
  engine.onAdd(s0);

  EXPECT_FALSE(engine.snapshot().asks().contains(h0.price));
  EXPECT_FALSE(engine.snapshot().asks().contains(h1.price));
}

TEST(MatchingEngineTest, OnAddHistoryNoMatch) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  MatchingEngine engine(&listener);

  AddMessage h0;
  h0.source = SOURCE_HISTORICAL;
  h0.orderNumber = 0;
  h0.side = Side::S;
  h0.quantity = 1000;
  h0.price = 10.00;

  AddMessage s0;
  s0.source = SOURCE_STRATEGY;
  s0.orderNumber = 100;
  s0.side = Side::B;
  s0.quantity = h0.quantity;
  s0.price = h0.price - 1.00;

  Order exp0(h0.orderNumber, h0.side, h0.quantity, h0.price);
  Order exp1(s0.orderNumber, s0.side, s0.quantity, s0.price);

  engine.onAdd(s0);
  engine.onAdd(h0);

  EXPECT_EQ(*engine.snapshot().asks().at(h0.price).front(), exp0);
  EXPECT_EQ(*engine.snapshot().bids().at(s0.price).front(), exp1);
}

TEST(MatchingEngineTest, OnAddHistoryPerfectMatch) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  MatchingEngine engine(&listener);

  AddMessage h0;
  h0.source = SOURCE_HISTORICAL;
  h0.orderNumber = 0;
  h0.side = Side::S;
  h0.quantity = 1000;
  h0.price = 10.00;

  AddMessage s0;
  s0.source = SOURCE_STRATEGY;
  s0.orderNumber = 100;
  s0.side = Side::B;
  s0.quantity = h0.quantity;
  s0.price = h0.price;

  engine.onAdd(s0);
  engine.onAdd(h0);

  EXPECT_FALSE(engine.snapshot().asks().contains(h0.price));
  EXPECT_FALSE(engine.snapshot().bids().contains(s0.price));
}

TEST(MatchingEngineTest, OnAddHistoricalMatchMultipleStrategy) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  MatchingEngine engine(&listener);

  AddMessage s0;
  s0.source = SOURCE_STRATEGY;
  s0.orderNumber = 0;
  s0.side = Side::S;
  s0.quantity = 1000;
  s0.price = 10.00;

  AddMessage s1;
  s1.source = SOURCE_STRATEGY;
  s1.orderNumber = 1;
  s1.side = Side::S;
  s1.quantity = 1000;
  s1.price = 10.00;

  AddMessage s2;
  s2.source = SOURCE_STRATEGY;
  s2.orderNumber = 2;
  s2.side = Side::S;
  s2.quantity = 1000;
  s2.price = 10.00;

  AddMessage h0;
  h0.source = SOURCE_HISTORICAL;
  h0.orderNumber = 100;
  h0.side = Side::B;
  h0.quantity = 2500;
  h0.price = 10.00;

  Order exp0(s2.orderNumber, s2.side, 500, s2.price);

  engine.onAdd(s0);
  engine.onAdd(s1);
  engine.onAdd(s2);
  engine.onAdd(h0);

  EXPECT_EQ(engine.snapshot().asks().at(s2.price).size(), 1);
  EXPECT_EQ(*engine.snapshot().asks().at(s2.price).front(), exp0);
  EXPECT_FALSE(engine.snapshot().bids().contains(h0.price));
}

TEST(MatchingEngineTest, OnAddHistoricalMatchPortionStrategy) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  MatchingEngine engine(&listener);

  AddMessage h0;
  h0.source = SOURCE_HISTORICAL;
  h0.orderNumber = 0;
  h0.side = Side::S;
  h0.quantity = 1000;
  h0.price = 10.00;

  AddMessage s0;
  s0.source = SOURCE_STRATEGY;
  s0.orderNumber = 100;
  s0.side = Side::B;
  s0.quantity = 2000;
  s0.price = h0.price;

  Order exp1(s0.orderNumber, s0.side, 1000, s0.price);

  engine.onAdd(s0);
  engine.onAdd(h0);

  EXPECT_FALSE(engine.snapshot().asks().contains(h0.price));
  EXPECT_EQ(*engine.snapshot().bids().at(s0.price).front(), exp1);
}

TEST(MatchingEngineTest, OnAddHistoryBidMatchLowerAsk) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  MatchingEngine engine(&listener);

  AddMessage s0;
  s0.source = SOURCE_STRATEGY;
  s0.orderNumber = 0;
  s0.side = Side::S;
  s0.quantity = 1000;
  s0.price = 20.00;

  AddMessage s1;
  s1.source = SOURCE_STRATEGY;
  s1.orderNumber = 1;
  s1.side = Side::S;
  s1.quantity = 1000;
  s1.price = 10.00;

  AddMessage h0;
  h0.source = SOURCE_HISTORICAL;
  h0.orderNumber = 100;
  h0.side = Side::B;
  h0.quantity = 2000;
  h0.price = 15.00;

  Order exp0(h0.orderNumber, h0.side, 1000, h0.price);
  Order exp1(s0.orderNumber, s0.side, s0.quantity, s0.price);

  engine.onAdd(s0);
  engine.onAdd(s1);
  engine.onAdd(h0);
  EXPECT_EQ(*engine.snapshot().bids().at(h0.price).front(), exp0);
  EXPECT_EQ(*engine.snapshot().asks().at(s0.price).front(), exp1);
}

TEST(MatchingEngineTest, OnAddHistoricalAskMatchHigherBid) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  MatchingEngine engine(&listener);

  AddMessage s0;
  s0.source = SOURCE_STRATEGY;
  s0.orderNumber = 0;
  s0.side = Side::B;
  s0.quantity = 1000;
  s0.price = 10.00;

  AddMessage s1;
  s1.source = SOURCE_STRATEGY;
  s1.orderNumber = 1;
  s1.side = Side::B;
  s1.quantity = 1000;
  s1.price = 20.00;

  AddMessage h0;
  h0.source = SOURCE_HISTORICAL;
  h0.orderNumber = 100;
  h0.side = Side::S;
  h0.quantity = 2000;
  h0.price = 15.00;

  Order exp0(h0.orderNumber, h0.side, 1000, h0.price);
  Order exp1(s0.orderNumber, s0.side, s0.quantity, s0.price);

  engine.onAdd(s0);
  engine.onAdd(s1);
  engine.onAdd(h0);

  EXPECT_EQ(*engine.snapshot().asks().at(h0.price).front(), exp0);
  EXPECT_EQ(*engine.snapshot().bids().at(s0.price).front(), exp1);
}

TEST(MatchingEngineTest, OnAddHistoricalMatchMultipleLevels) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  MatchingEngine engine(&listener);

  AddMessage s0;
  s0.source = SOURCE_STRATEGY;
  s0.orderNumber = 0;
  s0.side = Side::S;
  s0.quantity = 1000;
  s0.price = 10.00;

  AddMessage s1;
  s1.source = SOURCE_STRATEGY;
  s1.orderNumber = 1;
  s1.side = Side::S;
  s1.quantity = 1000;
  s1.price = 12.00;

  AddMessage h0;
  h0.source = SOURCE_HISTORICAL;
  h0.orderNumber = 100;
  h0.side = Side::B;
  h0.quantity = 2000;
  h0.price = 15.00;

  engine.onAdd(s0);
  engine.onAdd(s1);
  engine.onAdd(h0);

  EXPECT_FALSE(engine.snapshot().bids().contains(h0.price));
  EXPECT_FALSE(engine.snapshot().asks().contains(s0.price));
  EXPECT_FALSE(engine.snapshot().asks().contains(s1.price));
}

TEST(MatchingEngineTest, OnTradeOriginalMatch) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  MatchingEngine engine(&listener);

  AddMessage h0;
  h0.source = SOURCE_HISTORICAL;
  h0.orderNumber = 0;
  h0.side = Side::B;
  h0.quantity = 1000;
  h0.price = 10.00;

  AddMessage s0;
  s0.source = SOURCE_STRATEGY;
  s0.orderNumber = 100;
  s0.side = h0.side;
  s0.quantity = h0.quantity;
  s0.price = h0.price;

  engine.onAdd(h0);
  engine.onAdd(s0);

  TradeMessage t0;
  t0.source = SOURCE_HISTORICAL;
  t0.orderNumber = h0.orderNumber;
  t0.aggressedSide = h0.side;
  t0.quantity = h0.quantity;
  t0.price = h0.price;

  Order exp0(s0.orderNumber, s0.side, s0.quantity, s0.price);

  engine.onTrade(t0);
  EXPECT_EQ(*engine.snapshot().bids().at(s0.price).front(), exp0);
}

TEST(MatchingEngineTest, OnTradePerfectMatch) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  MatchingEngine engine(&listener);

  AddMessage h0;
  h0.source = SOURCE_HISTORICAL;
  h0.orderNumber = 0;
  h0.side = Side::B;
  h0.quantity = 1000;
  h0.price = 10.00;

  AddMessage s0;
  s0.source = SOURCE_STRATEGY;
  s0.orderNumber = 100;
  s0.side = h0.side;
  s0.quantity = h0.quantity;
  s0.price = 12.00;

  engine.onAdd(h0);
  engine.onAdd(s0);

  TradeMessage t0;
  t0.source = SOURCE_HISTORICAL;
  t0.orderNumber = h0.orderNumber;
  t0.aggressedSide = h0.side;
  t0.quantity = h0.quantity;
  t0.price = h0.price;

  Order exp0(h0.orderNumber, h0.side, h0.quantity, h0.price);

  engine.onTrade(t0);
  // TODO test the trade message correct price etc
  EXPECT_EQ(*engine.snapshot().bids().at(h0.price).front(), exp0);
}

TEST(MatchingEngineTest, OnTradeNoMatch) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  MatchingEngine engine(&listener);

  AddMessage h0;
  h0.source = SOURCE_HISTORICAL;
  h0.orderNumber = 0;
  h0.side = Side::S;
  h0.quantity = 1000;
  h0.price = 10.00;

  AddMessage s0;
  s0.source = SOURCE_STRATEGY;
  s0.orderNumber = 100;
  s0.side = h0.side;
  s0.quantity = h0.quantity;
  s0.price = h0.price + 0.50;

  engine.onAdd(h0);
  engine.onAdd(s0);

  TradeMessage t0;
  t0.source = SOURCE_HISTORICAL;
  t0.orderNumber = h0.orderNumber;
  t0.aggressedSide = h0.side;
  t0.quantity = h0.quantity;
  t0.price = h0.price;

  Order exp0(s0.orderNumber, s0.side, s0.quantity, s0.price);

  engine.onTrade(t0);
  EXPECT_FALSE(engine.snapshot().asks().contains(h0.price));
  EXPECT_EQ(*engine.snapshot().asks().at(s0.price).front(), exp0);
}

TEST(MatchingEngineTest, OnTradeMatchMultipleStrategy) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  MatchingEngine engine(&listener);

  AddMessage h0;
  h0.source = SOURCE_HISTORICAL;
  h0.orderNumber = 0;
  h0.side = Side::S;
  h0.quantity = 1000;
  h0.price = 10.00;

  AddMessage s0;
  s0.source = SOURCE_STRATEGY;
  s0.orderNumber = 100;
  s0.side = h0.side;
  s0.quantity = h0.quantity / 2;
  s0.price = 8.00;

  AddMessage s1;
  s1.copy(s0);
  ++s1.orderNumber;

  AddMessage s2;
  s2.copy(s1);
  ++s2.orderNumber;

  engine.onAdd(h0);
  engine.onAdd(s0);
  engine.onAdd(s1);
  engine.onAdd(s2);

  TradeMessage t0;
  t0.source = SOURCE_HISTORICAL;
  t0.orderNumber = h0.orderNumber;
  t0.aggressedSide = h0.side;
  t0.quantity = h0.quantity;
  t0.price = h0.price;

  Order exp0(h0.orderNumber, h0.side, h0.quantity, h0.price);
  Order exp1(s2.orderNumber, s2.side, s2.quantity, s2.price);

  engine.onTrade(t0);
  EXPECT_EQ(*engine.snapshot().asks().at(h0.price).front(), exp0);
  EXPECT_EQ(*engine.snapshot().asks().at(s0.price).front(), exp1);
}

TEST(MatchingEngineTest, OnTradeMatchPortionStrategy) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  MatchingEngine engine(&listener);

  AddMessage h0;
  h0.source = SOURCE_HISTORICAL;
  h0.orderNumber = 0;
  h0.side = Side::S;
  h0.quantity = 1000;
  h0.price = 10.00;

  AddMessage s0;
  s0.source = SOURCE_STRATEGY;
  s0.orderNumber = 100;
  s0.side = h0.side;
  s0.quantity = h0.quantity * 2;
  s0.price = 8.00;

  engine.onAdd(h0);
  engine.onAdd(s0);

  TradeMessage t0;
  t0.source = SOURCE_HISTORICAL;
  t0.orderNumber = h0.orderNumber;
  t0.aggressedSide = h0.side;
  t0.quantity = h0.quantity;
  t0.price = h0.price;

  Order exp0(s0.orderNumber, s0.side, s0.quantity - t0.quantity, s0.price);
  Order exp1(h0.orderNumber, h0.side, h0.quantity, h0.price);

  engine.onTrade(t0);
  EXPECT_EQ(*engine.snapshot().asks().at(s0.price).front(), exp0);
  EXPECT_EQ(*engine.snapshot().asks().at(h0.price).front(), exp1);
}

