#include <gtest/gtest.h>
#include "MockExchange.h"
#include "OrderBook.h"
#include "OrderBookException.hpp"

#define AUCT(event) static_pointer_cast<AuctMessage>(event)

TEST(OrderBookTest, EventOrdering) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  OrderBook book(&listener);

  AddMessage msg0;
  msg0.deliver_time = 0;
  msg0.deliver_time = msg0.deliver_time;
  msg0.orderNumber = 111;
  msg0.side = Side::B;
  msg0.quantity = 1000;
  msg0.price = 10.00;

  AddMessage msg1;
  msg1.deliver_time = 3;
  msg1.deliver_time = msg1.deliver_time;
  msg1.orderNumber = 112;
  msg1.side = Side::B;
  msg1.quantity = 1000;
  msg1.price = 10.00;

  UpdateMessage msg2;
  msg2.deliver_time = 1;
  msg2.deliver_time = msg2.deliver_time;
  msg2.orderNumber = 111;
  msg2.newOrderNumber = 222;
  msg2.quantity = 2000;
  msg2.price = 10.05;

  // Should be deliver_time ordered
  book.onAdd(msg0);
  book.onAdd(msg1);
  book.onUpdate(msg2);

  EXPECT_EQ(events.size(), 3);

  EXPECT_EQ(AUCT(events.top())->deliver_time, msg0.deliver_time);
  events.pop();

  EXPECT_EQ(AUCT(events.top())->deliver_time, msg2.deliver_time);
  events.pop();

  EXPECT_EQ(AUCT(events.top())->deliver_time, msg1.deliver_time);
  events.pop();
}

TEST(OrderBookTest, OnAdd) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  OrderBook book(&listener);
  Snapshot snapshot;

  // Add Single
  AddMessage msg0;
  msg0.deliver_time = 0;
  msg0.orderNumber = 111;
  msg0.side = Side::B;
  msg0.quantity = 1000;
  msg0.price = 10.00;
  Order exp0(msg0.orderNumber, msg0.side, msg0.quantity, msg0.price);

  book.onAdd(msg0);
  snapshot = book.snapshot();
  EXPECT_EQ(snapshot.bids().at(msg0.price).size(), 1);
  EXPECT_EQ(*snapshot.bids().at(msg0.price).front(), exp0);
  EXPECT_EQ(*AUCT(events.top()).get(), msg0);

  // Add Subsequent to End of List
  AddMessage msg1;
  msg1.deliver_time = 0;
  msg1.orderNumber = 112;
  msg1.side = Side::B;
  msg1.quantity = 2000;
  msg1.price = msg0.price;
  Order exp1(msg1.orderNumber, msg1.side, msg1.quantity, msg1.price);

  book.onAdd(msg1);
  snapshot = book.snapshot();
  EXPECT_EQ(snapshot.bids().at(msg1.price).size(), 2);
  EXPECT_EQ(*snapshot.bids().at(msg1.price).back(), exp1);
  EXPECT_EQ(events.size(), 2);
  events.pop();
  events.pop();

//  // Bad Order Number
//  AddMessage msg2 = msg0;
//  EXPECT_THROW(book.onAdd(msg2), OrderBookException);
//  EXPECT_TRUE(events.empty());
}

TEST(OrderBookTest, OnUpdateSingle) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  OrderBook book(&listener);
  Snapshot snapshot;

  AddMessage msg0;
  msg0.deliver_time = 0;
  msg0.orderNumber = 111;
  msg0.side = Side::B;
  msg0.quantity = 1000;
  msg0.price = 10.00;
  UpdateMessage msg1;
  msg1.deliver_time = 0;
  msg1.orderNumber = 111;
  msg1.newOrderNumber = 222;
  msg1.quantity = 2000;
  msg1.price = 10.05;

  Order expected(msg1.newOrderNumber, msg0.side, msg1.quantity, msg1.price);

  book.onAdd(msg0);
  book.onUpdate(msg1);

  snapshot = book.snapshot();
  EXPECT_TRUE(snapshot.bids().count(msg0.price) == 0 || snapshot.bids().at(msg0.price).size() == 0);
  EXPECT_EQ(snapshot.bids().at(msg1.price).size(), 1);
  EXPECT_EQ(*snapshot.bids().at(msg1.price).front(), expected);

  // Remove
  UpdateMessage msg2;
  msg2.deliver_time = 0;
  msg2.orderNumber = msg1.newOrderNumber;
  msg2.quantity = 0;
  msg2.price = msg1.price;

  book.onUpdate(msg2);
  snapshot = book.snapshot();
  EXPECT_TRUE(snapshot.bids().count(msg2.price) == 0 || snapshot.bids().at(msg2.price).size() == 0);

  // Bad Order Number
  UpdateMessage msg3;
  msg3.deliver_time = 0;
  msg3.orderNumber = 0;
  book.onUpdate(msg3);
}

TEST(OrderBookTest, OnUpdateMultiple) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  OrderBook book(&listener);
  Snapshot snapshot;

  AddMessage msg0;
  msg0.deliver_time = 0;
  msg0.orderNumber = 111;
  msg0.side = Side::B;
  msg0.quantity = 1000;
  msg0.price = 10.00;
  AddMessage msg1;
  msg1.deliver_time = 0;
  msg1.orderNumber = 112;
  msg1.side = Side::B;
  msg1.quantity = 1000;
  msg1.price = 10.00;
  AddMessage msg2;
  msg2.deliver_time = 0;
  msg2.orderNumber = 113;
  msg2.side = Side::B;
  msg2.quantity = 1000;
  msg2.price = 20.00;

  book.onAdd(msg0);
  book.onAdd(msg1);
  book.onAdd(msg2);

  // Same price position unchanged
  UpdateMessage msg3;
  msg3.deliver_time = 0;
  msg3.orderNumber = 111;
  msg3.newOrderNumber = 222;
  msg3.quantity = 2000;
  msg3.price = 10.00;
  Order exp0(msg3.newOrderNumber, msg0.side, msg3.quantity, msg3.price);

  book.onUpdate(msg3);
  snapshot = book.snapshot();
  EXPECT_EQ(snapshot.bids().at(msg3.price).size(), 2);
  EXPECT_EQ(*snapshot.bids().at(msg3.price).front(), exp0);

  // Different price end of list
  UpdateMessage msg4;
  msg4.deliver_time = 0;
  msg4.orderNumber = 222;
  msg4.newOrderNumber = 333;
  msg4.quantity = 1000;
  msg4.price = msg2.price;
  Order exp1(msg4.newOrderNumber, msg0.side, msg4.quantity, msg4.price);

  book.onUpdate(msg4);
  snapshot = book.snapshot();
  EXPECT_EQ(snapshot.bids().at(msg4.price).size(), 2);
  EXPECT_EQ(*snapshot.bids().at(msg4.price).back(), exp1);
}

TEST(OrderBookTest, OnCancelSingle) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  OrderBook book(&listener);
  Snapshot snapshot;

  AddMessage msg0;
  msg0.deliver_time = 0;
  msg0.orderNumber = 111;
  msg0.side = Side::B;
  msg0.quantity = 1000;
  msg0.price = 10.00;
  book.onAdd(msg0);

  // Not Remove
  // TODO better copy and initializers
  CancelMessage msg1;
  msg1.deliver_time = 0;
  msg1.orderNumber = msg0.orderNumber;
  msg1.quantity = 10;
  Order exp0(msg0.orderNumber, msg0.side, msg0.quantity - msg1.quantity, msg0.price);

  book.onCancel(msg1);
  snapshot = book.snapshot();
  EXPECT_EQ(*snapshot.bids().at(msg0.price).front(), exp0);

  // Remove
  CancelMessage msg2;
  msg2.deliver_time = 0;
  msg2.orderNumber = msg0.orderNumber;
  msg2.quantity = msg0.quantity - msg1.quantity;

  book.onCancel(msg2);
  snapshot = book.snapshot();
  EXPECT_TRUE(snapshot.bids().count(msg0.price) == 0 || snapshot.bids().at(msg0.price).size() == 0);

  // Remove Extra throws
  AddMessage msg3;
  msg3.deliver_time = 0;
  msg3.orderNumber = 888;
  msg3.side = Side::B;
  msg3.quantity = 1000;
  msg3.price = 10.00;
  book.onAdd(msg3);
  CancelMessage msg4;
  msg4.deliver_time = 0;
  msg4.orderNumber = msg3.orderNumber;
  msg4.quantity = msg3.quantity + 1;

  book.onCancel(msg4);

  // Bad Order Number
  CancelMessage msg5;
  msg5.deliver_time = 0;
  msg5.orderNumber = 0;
  msg5.quantity = 10;
  book.onCancel(msg5);

  // CANCEL_ALL
  AddMessage msg6;
  msg6.deliver_time = 0;
  msg6.orderNumber = 999;
  msg6.side = Side::B;
  msg6.quantity = 1000;
  msg6.price = 100.00;
  book.onAdd(msg6);
  CancelMessage msg7;
  msg7.deliver_time = 0;
  msg7.orderNumber = msg6.orderNumber;
  msg7.quantity = CANCEL_ALL;

  book.onCancel(msg7);
  snapshot = book.snapshot();
  EXPECT_TRUE(snapshot.bids().count(msg6.price) == 0 || snapshot.bids().at(msg6.price).size() == 0);
}

TEST(OrderBookTest, OnCancelMultiple) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  OrderBook book(&listener);
  Snapshot snapshot;

  AddMessage msg0;
  msg0.deliver_time = 0;
  msg0.orderNumber = 111;
  msg0.side = Side::B;
  msg0.quantity = 1000;
  msg0.price = 10.00;
  AddMessage msg1;
  msg1.deliver_time = 0;
  msg1.orderNumber = 112;
  msg1.side = Side::B;
  msg1.quantity = 2000;
  msg1.price = msg0.price;
  book.onAdd(msg0);
  book.onAdd(msg1);

  // Not Remove
  CancelMessage msg2;
  msg2.deliver_time = 0;
  msg2.orderNumber = msg0.orderNumber;
  msg2.quantity = 10;
  Order exp0(msg0.orderNumber, msg0.side, msg0.quantity - msg2.quantity, msg0.price);

  book.onCancel(msg2);
  snapshot = book.snapshot();
  EXPECT_EQ(*snapshot.bids().at(msg0.price).front(), exp0);

  // Remove
  CancelMessage msg3;
  msg3.deliver_time = 0;
  msg3.orderNumber = msg0.orderNumber;
  msg3.quantity = msg0.quantity - msg2.quantity;

  Order exp1(msg1.orderNumber, msg1.side, msg1.quantity, msg1.price);

  book.onCancel(msg3);
  snapshot = book.snapshot();
  EXPECT_EQ(snapshot.bids().at(msg0.price).size(), 1);
  EXPECT_EQ(*snapshot.bids().at(msg0.price).front(), exp1);
}

TEST(OrderBookTest, OnTrade) {
  MockExchange listener;
  EventQueue& events = listener.getEvents();
  OrderBook book(&listener);
  Snapshot snapshot;

  AddMessage msg0;
  msg0.deliver_time = 0;
  msg0.orderNumber = 111;
  msg0.side = Side::B;
  msg0.quantity = 1000;
  msg0.price = 10.00;

  TradeMessage msg1;
  msg1.deliver_time = 0;
  msg1.orderNumber = 111;
  msg1.aggressedSide = Side::B;
  msg1.matchNumber = 222;
  msg1.quantity = 50;
  msg1.price = 10.00;

  book.onAdd(msg0);
  book.onTrade(msg1);
  snapshot = book.snapshot();
  EXPECT_EQ(snapshot.bids().at(msg0.price).front()->quantity, msg0.quantity - msg1.quantity);
}

