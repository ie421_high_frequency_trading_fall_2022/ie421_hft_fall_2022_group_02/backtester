const express = require('express')
const app = express()
const sqlite3 = require('sqlite3').verbose();
const port = 8080
const cors = require('cors');
let db = new sqlite3.Database(process.env.DB_PATH);

app.use(cors({
    origin: '*'
}));

app.get('/', (req, res) => {
  res.sendFile("./index.html", {root: __dirname })
})


app.get('/trades/:start/:end', (req, res) => {
    let sql = `SELECT * FROM TRADES
    WHERE  timestamp > ${req.params.start} AND timestamp < ${req.params.end}
    ORDER BY timestamp`;
    db.all(sql, [], (err, rows) => {
        if (err) {
            throw err;
        }
        
        try {
            let start = Math.min(...rows.map((v) => v["TIMESTAMP"]))
            let end = Math.max(...rows.map((v) => v["TIMESTAMP"]))
            let span = Math.ceil((end - start) / 100)
            data = rows.reduce((agg, v) => {
                let k = Math.floor(v["TIMESTAMP"] / span) * span
                agg[k] = agg[k] || []
                agg[k].push(v)
                return agg
            }, {})
            data = Object.values(data)
            res.send(data.map((v) => {
                return {time: v[0]["TIMESTAMP"], open: v[0]["PRICE"], high: Math.max(...v.map(t=>t["PRICE"])), low: Math.min(...v.map(t=>t["PRICE"])), close: v[v.length - 1]["PRICE"] }
            }).reduce((a, v) => {a.push(v); return a;}, []))
        } catch (err) {
          res.send({err: "Range too big for prices to be displayed"})
        }
    });
})

app.get('/position/:start/:end', (req, res) => {
    let sql = `SELECT * FROM POSITION
    WHERE  time > ${req.params.start} AND time < ${req.params.end}
    ORDER BY time`;
    db.all(sql, [], (err, rows) => {
        if (err) {
            throw err;
        }
        res.send([...rows.map((r) => {return {time: r["TIME"], pnl: r["PNL"], cash: r['CASH'], share: r["SHARES"]}})])
    });
})

app.get('/trades/all', (req, res) => {
    db.all("SELECT timestamp, price from TRADES", [], (err, rows) => {
        if (err) {
            throw err;
        }
        res.send(rows)
    });
})

app.get('/orders/:time', (req, res) => {

    let sql = `SELECT * FROM ORDERS 
            WHERE start < ${req.params.time} AND end > ${req.params.time}
            ORDER BY PRICE, PRIORITY`;

    db.all(sql, [], (err, rows) => {
    if (err) {
        throw err;
    }
    res.send(rows)
    });
})

app.get('/formatted-orders/:time', (req, res) => {

    let sql = `SELECT * FROM ORDERS 
            WHERE start < ${req.params.time} AND end > ${req.params.time}
            ORDER BY PRICE, PRIORITY`;

    db.all(sql, [], (err, rows) => {
    if (err) {
        throw err;
    }
    prices = [...rows.map((r) => {return r["PRICE"];})]
    data = {}
    for (let i = 0; i < prices.length; i++) {
        level = {}
        level.type = "stackedColumn"
        level.name = prices[i]
        level.dataPoints = []
        data[prices[i]] = level
    }
    for (let i = 0; i < rows.length; i++) {
        level = data[rows[i]['PRICE']]
        level.dataPoints.push({x: level.name, y: rows[i]["QUANTITY"]})
    }
    res.send(Object.values(data))

    });
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
