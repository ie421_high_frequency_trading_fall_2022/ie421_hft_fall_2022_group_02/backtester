# Backtester

## A backtesting framework and order matching algorithm written in C++

## Team

### (Team Leader) Nairen Fu (nairenf2@illinois.edu)

Nairen is a current Senior at the University of Illinois at Urbana-Champaign majoring in Computer Science. Nairen is graduating May 2023. Nairen enjoys working with the interaction and interoperability between systems.

### Bohan Liu (bohan3@illinois.edu)

![implementation](/assets/BL_profile.png)  
Bohan is a current Senior at the University of Illinois at Urbana-Champaign majoring in Computer Science. Bohan is graduating May 2023. Bohan intends to pursue a Master of Computer Science at University of Illinois at Urbana-Champaign starting the Fall of 2023. Bohan enjoys working on database, web-backend as well as machine learning and artificial intelligence.

### Pavan Otthi (potthi2@illinois.edu)

Pavan is a current senior at the University of Illinois at Urbana-Champaign majoring in Computer Engineering. Pavan is graduating May 2023. Pavan focuses his work on the intersection of machine learning and finance, specifically building feature engineering infrastructure.

## Project

### Description

This is the final report for our semester long project for IE421 - High Frequency Trading under [Professor David Lariviere](https://davidl.web.illinois.edu).

We have designed and implemented a backtesting framework akin to what a High Frequency Trading (HFT) firm may have to test various strategies (albeit much simpler). In gist, we consume Add, Update, Cancel, and Trade (called AUCT collectively) level 3 messages from historical market exchange data. We then replay this historical data, and reconstruct the state of the order books. A user implemented strategy can then trade against this historical data using the hooks that we have provided. Finally, the user will be able to analyze the market conditions with their strategy in place, as well as analyze their Profit & Loss ratio to identify bugs in their strategy and where they can be improved.

The intent of this backtester is to provide a risk-free, approachable, consistent, and fast development cycle for traders to develop, test, and optimize new strategies. With a backtester, a trader will be able to test their strategy in a simulated environment against historical data, which aims to provide a realistic scenario that does not involve any real money until they are confident if this strategy. With a standardized interface that abstracts away the details (such as actually sending the orders or parsing the bytestring messages), it is easier for traders to iterate on their algorithms. Furthermore, by being able to simulate the data much faster than real-time, the development cycle is shortened significantly. The combination of the aforementioned hopefully encourages traders to experiment and test more, hopefully leading to more competitive and less error-prone strategies.

We chose to do this project as we believe that a tool like a backtester can be a significant contributor to the ultimate performance of a firm's strategies as a whole. Furthermore, this project encompasses a significant portion of an HFT firm's pipeline, from data ingestion to parsing, trading, and visualization.

### Technologies

The various components of our project is packaged within a [Vagrant](https://www.vagrantup.com) environment. A Vagrant environment is essentially a self-contained operating system, in which our programs will run. This separation of concern streamlines the setup procedure, standardizing and taking care of any necessary environment variables and prerequisites, allowing developers to focus on features and users on application. Docker was also considered. However, since we want to run our program in a persistent VM for easier development (as compared to restarting a Docker container after each modification), we settled on Vagrant.

Our core backtester program is written in C++20. We are using C++ for speed, and the 2020 revision of the standard to use some of the newer features such as `ranges`. We also considered using Rust or Golang, but we stuck to C++ due to greater familiarity. We are using [CMake](https://cmake.org) to manage the building of our project. In terms of libraries, we are using the [GoogleTest](http://google.github.io/googletest) library as our unit testing framework, and the [SQLite3](https://www.sqlite.org/cintro.html) library to save order information to an SQLite database.

Our data management application is Sqlite3. This decision is based on the scope and performance of our project. We need efficient indexed data structure to record full-resolution data from the backtesting run. Yet we need efficiency in running database operations. A in-memory sqlite database that can be dumped to a file after each run best suits our needs.

Our web backend for result delivery is Node.js. Node is one of the industry-standard backend for web applications. It's developing workflow is simple and straight-forward, and it is well-supported with packages for app structure and database connection. So it suits our need for developing a light-weight local web app.

Our web frontend is jQuery and Canvas.js. Canvas.js provides stock charts for use and various customizable functionalities. It has a jQuery varient. Since we are making a light-weight single page app, it is the easiest way to do it.

### Components

#### Event Framework and Management

The general structure of the application is as follows.
![structure](/assets/EventStructure.png)  
There are two threads running: 1. the fetcher thread runs the data loader and load data for consumption. 2. the runner thread runs the exchange and the strategy.

- Timemanager is a class providing function calls to fetcher thread and runner thread to provide synchronization.
- Backtester is a class that runs on runner thread. It keeps track of the time and next event to fire. It also calls the dumper to record the event.
- Data loader is the CSV interface for csv generated by this program: [ITCH](https://github.com/martinobdl/ITCH).
- Event dumper is the sqlite interface to record all events into our desired format. Our SQL schemas is the following:

```
CREATE TABLE TRADES(
    USER          INT     NOT NULL,
    TIMESTAMP      LONG    NOT NULL,
    QUANTITY       INT     NOT NULL,
    PRICE          REAL     NOT NULL,
    AGRESSED       INT);

CREATE TABLE ORDERS(
    ID INT    NOT NULL,
    START      LONG    NOT NULL,
    END      LONG    NOT NULL,
    PRIORITY LONG NOT NULL,
    QUANTITY       INT     NOT NULL,
    PRICE          REAL     NOT NULL,
    SIDE       INT NOT NULL,
    PRIMARY KEY (ID, START) );

CREATE TABLE POSITION(
    TIME LONG,
    SHARES REAL,
    CASH REAL,
    PNL REAL );
```

#### Exchange

##### Implementation

Like an onion, an `Exchange` contains a mapping of `MatchingEngine`s, which each contain an `OrderBook` for the specific `stockLocate`.
![implementation](/assets/exchange_implementation.png)

The `Exchange` is the sole interface for the strategy to interact with the simulated market through the `Backtester`. With the event queue based structure, incoming messages are supplied by the `Backtester` while outgoing messages is added to its own outgoing messages queue. Each `MatchingEngine` in the `Exchange` manages the orders relating to a single instrument, identified via the `stockLocate`. Internally, the generic incoming messages gets casted to the precise AUCT message type in the `Exchange`, and gets is dispatched to the `OrderBook`. The `MatchingEngine` treats historical `TradeMessage`s as `AddMessage`s, which is described below in the Matching section. Finally, the `OrderBook` maintains the state of the book for a specific instrument. The actual matching is done within the `OrderBook` itself, with the `MatchingEngine` as a `friend` giving access to the `protected trade` interface. From a separation of concerns point of view, it should be the `MatchingEngine`'s responsibility to look up the `OrderBook` and perform the trades. However, it is far more memory and latency efficient for the `OrderBook` to perform this function since we do not want to expose all of its private members to maintain good organization and security. As such, we chose to perform the actual matching in the `OrderBook`.

##### Lifecycle

When an `event` comes in,

1. the exchange checks the type and gets casted to the correct `Message` type, and
1. gets dispatched to the correct interface function for the `MatchingEngine` with the corresponding `stockLocate`.
1. The `engine` forwards the `message` to the `OrderBook` which
1. updates the bids and asks (for less lookups and faster accesses).
1. If any resulting events (e.g. trade was performed) was emitted, it will be added to the `pending_events` queue of the `Exchange`, which is a priority queue by `deliver_time` of outgoing `Event`s.  
   ![lifecycle](/assets/exchange_lifecycle.png)

##### Matching

We implement a custom simple matching algorithm to balance correctness and time required for development. In the usual setting (to generate order books from market data), no real matching is required - when an `AddMessage` is received, an order is added, etc., and when a `TradeMessage` is received, the resting `Order` has its quantity reduced. However, we need to contend with `Message`s from both the historical data, and from the backtested strategy. The most optimal solution will be for strategy orders to shadow lower priority historical orders, but this is complicated (it can be a one strategy to many historical or vice versa, and can change due to `UpdateMessage`s). Given the limited time available, we decide to go with the more straightforward solution. The current implementation is:

- Priority is by best price then earliest time `Add`ed.
- Historical `TradeMessage`s get converted to `AddMessage`s. This works because if a new order can be matched, it will be matched immediately, and a `TradeMessage` will be sent instead of (and not in addition to) an `AddMessage`. So in the absence of strategy messages, this will just replay historical data.
- Matches between historical and strategy orders occur destructively - both orders are removed from the book. We debated this implementation for quite a bit, but this seemed like the best balance between correctness for the book and for the strategy. However, there is a major limitation - if the exchange chooses to match using a different algorithm, this would not be a very accurate simulation. Annoyingly, this information is not always available, so the only good solution would be to observe the difference in the actual number of trades versus the number of simulated trades for a set of historical data.

#### Strategy

The strategy extends the EventListener class.

When an event comes in,

**Lifecycle:**
![structure](/assets/interfacefinal.png)  
The strategy interface checks the type and gets casted to the correct Message type. After checking the predetermined latency, the strategy function is called. Based on the message type the appropriate function is called. On trade messages, the strategy checks to see if the strategy was involved in the trade and then adds the trade to its personal (strategy) trade history. The strategy also adds trades to keep track of the overall trade history. Depending on the event-based tick type that the user has selected, the strategy will generate the ticks if the conditions are met. The interface is designed to be flexible, allowing the user to design his implementation specifically around what is coming from the “gateway”. The strategy also computes backtest statistics like the sharpe ratio and the frequency of bets.

**Interface-Backtester**:
As explained earlier, the interface extends the EventListener class to communicate with the backtester. This interaction represents both a gateway and a tickerplant. The gateway portion is where the interface allows the strategy to place, update, or cancel orders. The tickerplant portion is how the backtester sends market data to the strategy.

**Strategy-Interface**:
The interface provides the user with key information necessary for strategy building, event-based tick bars generation, as well as key backtest statistics.

Event-Based tick bars:
(1) Time Bars
(2) Tick Bars
(3) Volume Bars
(4) Dollar Bars
(5) Tick-Imbalance Bars

**Time Bars**:

Should be avoided because the market doesn't process information at a fixed time interval so one may oversample during periods of low activity and undersample during periods of high activity

_Tick Bars_:

Sample based on the number of transactions (orders in this case)

_Volume Bars_:

Sample based on the number of lots

_Dollar Bars_:

Sample based on predefined market value

_Tick-Imbalance Bars_:

Sample relative to the amount of information arriving to the market.

**Backtest Statistics**:

_Sharpe Ratio_:
Calculated as the mean of the returns divided by the standard deviation.

_Average AUM_:
Represents the average dollar value of assets under management

_Leverage_:
Represents the amount of borrowing necessary to achieve the reported performance. In this case the leverage is measured as the ratio of the average dollar position size to the AUM.

_Maximum Dollar Position Size_:
Informs the user of the number of times the strategy took dollar positions that significantly exceeded the AUM statistic

_Frequency of Bets_:
Sequences of positions on the same side are counted as one "bet". The idea here is that counting trades leads to an overestimate of the number of independent opportunities detected by the strategy.

_Average Holding Period_:
Average amount of time that a position is held.

#### Visualization

The Visualization component is a separate web application that runs on its own. It relies on the dumped sqlite database only. This app will give the user a adjustable view on everything happened during backtesting. It has three components:

- Price OHLC chart.
- PnL and position line graph.
- Order Book visualization.

With this three components the user can get a comprehensive view of how their stratrgy responsed to various events in history to the very detail.

### Git Repo Layout

```
backtester
├── assets
├── data
├── src
│   ├── data
│   ├── main
│   ├── ob
│   └── strategy
├── tests
│   ├── main
│   ├── ob
│   └── src
├── vagrant
└── web
```

- Files in the root `backtester` directory are project-wide files.
- Files in `assets` are documentation related assets.
- Files in `data` are input data files or generated output files.
- Files in `vagrant` are Vagrant scripts that facilitate development, such as recompiling the source code.
- The project files are split into two essentially mirrored directories, `src` for source files and `tests` for test files.
  - `data` contains classes that deal with parsing and saving data
  - `main` contains classes that manage the lifecycle of the program
  - `ob` contains classes that make up the simulated exchange and its components (matching engine, order books, message structures, etc.)
  - `strategy` contains classes providing strategy interfaces and sample implementations
  - `tests/src` contains utility and mock classes that test cases use
- Files in `web` make up the web app that displays the order history of the simulation run.

### Instructions for Use

#### Dependencies

On the host device, the only dependency required is [Vagrant](https://www.vagrantup.com). The name of the Vagrant environment for this project is `backtester`.

#### Setup

```bash
git clone https://gitlab.engr.illinois.edu/ie421_high_frequency_trading_fall_2022/ie421_hft_fall_2022_group_02/backtester.git
cd backtester
vagrant up
```

#### Applying new Strategies

To implement a new strategy,

1. extend it from `src/strategy/StrategyInterface.h`, and use the available hooks to receive and process data.
2. Add it to the `strategy_array` in `src/main.cpp`. The first parameter is the name or a short description of the strategy, and the second is a pointer to the strategy itself.
3. Recompile the source code.

```bash
vagrant provision --provision-with compile
```

#### Running

This runs the backtesting simulation on the strategy you have specified.

```bash
vagrant ssh backtester -c "[(int) strategy index] [(path) input historical CSV] [(path) output trade CSV] [(path) output orders DB]"
```

By default, the output trade CSV writes to `data/events.csv` and the output orders DB writes to `data/dev.db`.

#### Analysis

This starts the web app from which you can analyze the orders during the simulation period.

```bash
vagrant ssh backtester -c "DB_PATH=<path_to_dumped_file> node web/dev.js"
```

The web app is located at [http://localhost:3000/](http://localhost:3000/).  
Accessing this url will show the user a view like this:  
To see this view:

1. Adjust the timespan using the navigator in the middle.
2. Click on the stock chart to select a timestamp to display orderbook.
3. press, hold and release on the orderbook visualization to zoom in.
4. press spack key to reset orderbook view.
   ![example](/assets/example.png)

#### Teardown

```bash
vagrant halt
vagrant destroy
```

#### Testing

To run the unit tests, run

```bash
vagrant ssh backtester -c "tests --gtest_color=yes"
```

All tests should pass.

As an integration test, you can run it on the provided dataset (1 day's worth of data for AAPL on NASDAQ).

```bash
vagrant ssh backtester -c backtester
vagrant ssh backtester -c "DB_PATH=/vagrant/data/dev.db node web/dev.js"
```

Visit [http://localhost:3000/](http://localhost:3000/). After adjusting the time range, you should see a graph similar to this:
![example stockchart](/assets/example.png)

### Results

At the end of our project, we have implemented a backtesting framework that simulates the interaction between a single exchange and a single strategy. We are able to extract key information like the orders, prices, and the profit and loss ratio of the strategy and visualize them.

In terms of the implementation of the matching algorithm, we can be confident that it is a fairly reasonable model of how NASDAQ implements it. With our custom matching, for a set of historical data for APPL on NASDAQ, we obtain somewhere in the range of 53000 trades, while the original data had 59822 trades.

We are fairly successful in the performance side of things as well. It takes less than 30s to run a day's worth of data against a simple strategy on laptop hardware. The biggest limiter of the speed is the strategy itself. With our implementation, since we load the data separately, it could be extended to support running multiple exchanges simultaneously as well. This leads to interesting implications and challenges - for example, a potential subsequent goal could be to run several strategies on the same exchange and see how they interact.

### Postmortem

#### Nairen Fu

For the core components of the project, I largely worked on the exchange side of things. However, beyond that, I performed plenty of project-wide roles as well. I set up the Vagrant environment, and established the unit testing framework. As the project leader, I took point in team organization as well, scheduling meetings, keeping track of progress, and helping teammates.

From the project, I learnt a lot about the data pipeline that an HFT firm may have - from how data might be ingested and processed to how they are used. It was also a good practice for C++, and learning more about continuous integration and continuous development, such as the technologies used (like Vagrant) and the best practices (like testing suites).

However, beyond the technical takeways, there were many significant organizational lessons learnt as well - namely how hard it is to get a team to collaborate on a project.

1. More time should be spent on planning and design than implementation. Going straight into implementation with just a vague idea of what one will be doing will most definitely lead to much more time being spent on the problem. Furthermore, it makes it difficult for other team members to anticipate your progress or understand what you are doing and how it relates to their components.
2. There needs to be a strong emphasis on task parallelism. The benefit of working in a team is that multiple problems can be worked on simultaneously. However, that is not possible if what people are doing collides or have dependencies. As such, good interfaces must be developed between components (something that is only possible if you nail point 1). This enforces separation of concerns, and allows you to treat others' components as black boxes, and know that it will work out because the shared interface is mutually agreed upon.
3. One needs to understand tradeoffs - neither time or effort is unlimited. It is important to know what your goals and priorities are, and how much resources you have and are willing to allocate to each problem. In our case specifically,
   - we used an external library to parse NASDAQ ITCH binary files to CSVs, since that was not the focus
   - we implemented a simple version of order matching, since it was more important to have something working first
4. Keep track of what you are doing, e.g. documentation and designs, preferrably in written form. If not to make it easier and clearer for you to update what you have done for the week, this at least makes it easier to write the report at the end.

In terms of further developments, there are plenty of enhancements that can be made.

- We should use a more sophisticated order shadowing method of matching, where strategy orders replace existing orders of lower priority, which can much more accurately model different exchanges.
- It would be good to use a library like [pybind11](https://pybind11.readthedocs.io/en/stable/) to create a Python interface for the strategy, resulting in both the benefits of a fast C++ based simulation, but the ease of implementing a strategy in Python.
- We could introduce more parallelism, i.e. running many strategies simultaneously to increase throughput. This also enables a machine driven development process, where multiple instances of the same algorithm is ran at the same time only with tweaks to the parameters to figure out the best performing set.
- We could implement more analysis tools - preferrably as an API so that traders could use the data however suits them the best.

#### Bohan Liu

I worked on the following:

- CSV dataloader
- SQL dumper
- vagrant provision file for setup sqlite3, node, and host port passthrough.
- Synchronization mechanism, Timemanager and its interaction with other components.
- EventListener and Backtester.
- Node.js backend.
- Canvas frontend migration and customization.

---

I learned a lot about C++ design patterns and smart memory management. Also, I practiced with sqlite3 and its C++ interface. I need to think through the interaction between threads in our app. And I practiced with C++ multithreading and concurrency apis. I searched and practiced full-stack web app development with a focus on trade data visualization. And I consolidated my knowledge on interactive website design.

---

One thing I will change in this project is the order matching mechanism. I proposed to shadow existing orders to make up some volume traded by the strategy, and our group had multiple discussions on it. I currently observe some orders with negative quantities on the book and possibly conflicting rest orders, which could be solved by a strict shadow policy.

Other thing to add would be to enable multiple symbols in our backtester. As Serena dropped I had to make-up the work in developing data ingestion pipeline. I reduced the workload by using existing ITCH parser, but it only support single symbol. We can expand our program to merge multiple CSVs or just develop our own ITCH parser if I had more time.

I would also reconsider the use of multi-threading in our app. The data loading process completes about 2-3x faster than the main backtester. I find that the backtester process is hard to decompose into more threads handling different parties. If parsing and merging multiple symbol data does not add a lot of computation, we can migrate to single-threaded design.

---

Speaking about improvements, it will be great if the final visualization can be something like [BookMap](https://bookmap.com/). Or if they have exposed api to provide data to their app, it will be useful to export data in such format.

Supporting multiple symbols or even more formats can be a direction for improvements.

The exchange need to limit the activity of the strategy. Now a single running strategy is moving the market quite a bit.

I plan to separate the inbound event for the exchange into event from strategy and events from history. Mixing the two, as we are currently doing, does not add much value to development.

---

It is very important to think everything through before you get started coding. We were thinking about a three-threaded design with possibly multiple strategy threads listening to the same history feed. I had to think that through and realize that it is not possible unless either strategy or history runs (which is just like an one-threaded model). We could ended up in developing into a deadend if we start before we think through.

Also, as professor stated, use anything available you can find. Using open-source programs saves you the time and work to deliver the core functionalities of your project.

I have a habit to prepare as if anyone in the group can drop the class. Therefore we can deliver in the midpoint demo, as I developed the csv parser to substiture the unfinished ingenstion component. I don't mean that everyone should all do the entire project, but just get prepared by searching for quick solutions to a certain component if you feel someone will not deliver in time.

#### Pavan Otthi

I mainly worked on the strategy implementation, designing how the strategy takes in data from the exchange, building the interface where the user can design their own strategy that will submit orders to the backtesting framework, and the overall backtester as well when we were investigating how the strategy was sending orders and matching.

From a technical perspective, with working on the project I was exposed to learning c++ design and best practices while using virtual machine software like vagrant. Being relatively inexperienced with c++ at the start of the project, I learned the importance of implementing my features alongside paying attention to and adjusting for changes in the other parts of the backtester. From a financial perspective, I learned how event based ticks are designed and what backtest statistics users would use to analyze how well their strategy performed.

From a nontechnical perspective, designing a framework from the ground up in a group setting definitely delivered some lessons learned. My biggest takeaway is the importance of making sure that the core component of my portion of the project is being developed in a way where the interaction with other components is finished first before diving deep into that implementation. If I had a time machine and could go back, I would have focused on integrating the strategy interface with the exchange and orderbook earlier. I also learned the importance of having the discussions necessary to come up with a strong design before coding to prevent what I explained previously from happening.

Further developments I would make to the product would include:

Designing an order matching algorithm that is non-deterministic and based on the probability a certain order would get matched, generating the liquidity for the order on the exchange side to limit the bias from the impact the orders from the strategy have on the data if orders placed are not expected to impact the market significantly. This is for trading strategies that are more complicated and take in a multitude of factors as the main question at hand is what financial features represent how the market will behave the best.

Building the interface that takes the returns generated from different strategies and using machine learning and feature engineering algorithms to figure out which alpha’s represented the market the best and when (what financial regime the market is in).

Extending the backtester to use data for multiple commodities, something that warrants changes in the way messages are transmitted between the exchange, orderbook, and strategy components

Use a library like pybind to allow the user to develop their strategy in python.

Some advice I would offer to future students is that it doesn't hurt to ask your group mates for help when you are stuck on a problem. This is something I am looking to be better at as I think it would have saved myself a lot of time instead of figuring things out myself.
