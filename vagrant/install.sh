#!/bin/sh

apt-get update

snap install cmake --classic
apt-get -y install build-essential g++-10 libsqlite3-dev

# install node
wget https://nodejs.org/dist/v18.12.1/node-v18.12.1-linux-x64.tar.xz && tar xf node-v18.12.1-linux-x64.tar.xz
cp -r ./node-v18.12.1-linux-x64 /usr/lib

ln -s /usr/lib/node-v18.12.1-linux-x64/bin/npm   /usr/local/bin/ 
ln -s /usr/lib/node-v18.12.1-linux-x64/bin/node   /usr/local/bin/

rm -r node-v18.12.1-linux-x64
rm node-v18.12.1-linux-x64.tar.xz

cp -r /vagrant/web .
npm install --prefix ./web

