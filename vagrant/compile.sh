#!/bin/sh

(
	mkdir -p build;
	cd build;
	export CC=gcc-10;
	export CXX=g++-10;
	cmake -DCMAKE_BUILD_TYPE=Debug /vagrant;
	make -j4;
	cp -r bin/* /usr/local/bin;
)
