#pragma once
#include <list>
#include <queue>
#include <map>
#include <memory>
#include "Event.h"

class Order;
class AuctMessage;
class AuctCompare;
typedef std::list<std::shared_ptr<Order>> PriceLevel;
typedef std::shared_ptr<std::map<double, PriceLevel>> MarketSidePtr;
typedef std::priority_queue<std::shared_ptr<AuctMessage>, std::vector<std::shared_ptr<AuctMessage>>, AuctCompare> MessageQueue;

class EventCompare {
public:            
  bool operator()(const std::shared_ptr<Event>& a, const std::shared_ptr<Event>& b) {
    return a->deliver_time > b->deliver_time;
  }
};

typedef std::priority_queue<std::shared_ptr<Event>, std::vector<std::shared_ptr<Event>>, EventCompare> EventQueue;

