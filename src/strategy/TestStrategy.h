
#pragma once
#include "StrategyInterface.h"
#include <tr1/unordered_map>
class TestStrategy : public StrategyInterface
{
public:
    void onEvent(std::shared_ptr<Event> event);
    void Strategy();
    long firstAverage;
    long secondAverage;
    std::tr1::unordered_map<int, int> orders;

    void printOrders();
};