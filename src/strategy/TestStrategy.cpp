
#include "TestStrategy.h"
#include "Order.h"
#define ADD(event) static_pointer_cast<AddMessage>(event)

void TestStrategy::printOrders()
{
    for (auto const &pair : orders)
    {
        std::cout << "{" << pair.first << ": " << pair.second << "}\n";
    }
}

void TestStrategy::onEvent(std::shared_ptr<Event> event)
{
    // std::cout << event->type << std::endl;
    currentTime = event->deliver_time;
    if (latency <= 0)
    {
        latency = 1;

        // if (event->type == Type::A){

        // }

        TestStrategy::Strategy();
        switch (event->type)
        {
        case Type::A:
        {
            const AddMessage &msg = *static_pointer_cast<AddMessage>(event);
            // std::cout << msg.source << ": " << msg.price << std::endl;

            this->onAdd(msg);
        }
        break;
        case Type::U:
        {

            const UpdateMessage &msg = *static_pointer_cast<UpdateMessage>(event);

            this->onUpdate(msg);
        }
        break;
        case Type::C:
        {

            const CancelMessage &msg = *static_pointer_cast<CancelMessage>(event);

            this->onCancel(msg);
        }
        break;
        case Type::T:
        {

            const TradeMessage &msg = *static_pointer_cast<TradeMessage>(event);

            this->onTrade(msg);

            // std::cout << "TRADE:" << " " << msg.price << std::endl;
        }
        break;
        }

        // if (event->type == Type::A && _pending_events.size() != 0 && _pending_events.size() <= 10)
        // {
        //     std::cout << "____queuecheck_____" << std::endl;
        //     std::cout << ADD(get())->orderNumber << " " << ADD(get())->price << std::endl;
        //     std::cout << "____queuecheck_____" << std::endl;
        // }
    }
    else
    {

        latency -= 1;
    }
}

void TestStrategy::Strategy()
{

    // Order o = Order(lastOrdernumber - 1, B, 10, 300.0);
    // placeOrder(o);
    // if (lastOrdernumber % 2 == 0)
    // {

    //     Order o = Order(lastOrdernumber - 1, S, 10, 100.0);
    //     placeOrder(o);
    // }
    // else
    // {
    //     Order o = Order(lastOrdernumber - 1, B, 10, 100.0);
    //     placeOrder(o);
    // }

    double sum = 0;
    if (orderbookBids.size() >= 1)
    {

        firstAverage = 0;
        secondAverage = 0;
        for (int i = 1; i <= 5; i++)
        {

            sum += orderbookBids[orderbookBids.size() - i].price;
        }

        firstAverage = sum / 5.0;
        sum = 0;
        for (int i = 5; i <= 10; i++)
        {

            sum += orderbookBids[orderbookBids.size() - i].price;
        }

        secondAverage = sum / 5.0;
        // std::cout << firstAverage << "  " << secondAverage << std::endl;
        if (firstAverage <= secondAverage)
        {

            Order o = Order(lastOrdernumber - 1, B, 10, orderbookAsks[orderbookAsks.size() - 1].price);
            if (orderbookAsks[orderbookAsks.size() - 1].price != 0)
            {
                placeAsk(orderbookAsks[orderbookAsks.size() - 1].price, 10);
            }
            // std::cout << o << std::endl;
        }
    }
}