#pragma once
#include "StrategyInterface.h"

class SimpleStrategy : public StrategyInterface
{
public:
    void Strategy() override;
};
