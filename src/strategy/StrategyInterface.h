#pragma once
#include "IStrategy.h"
#include "types.hpp"
#include "messages.h"
#include <iostream>
#include "Order.h"
#include <vector>
#include <map>
#include "Tick.h"
#include <cstdlib>
class StrategyInterface : public IStrategy
{

public:
    // TOPLEVEL: FOR STRATEGY BUILDING (THESE FUNCTIONS WOULD BE EXPORTED WITH PYBIND)
    virtual void Strategy();
    void placeOrder(double price, int quantity, int i);
    virtual void setInitialAmount(double initial);
    void placeAsk(double price, int shares);
    void placeBid(double price, int shares);
    std::map<int, int> getPlacedOrders();
    std::vector<TradeMessage> getTrades();
    long getCurrentShares();
    void cancelOrder();
    double getNetProfit();
    void SetReturnsFrequency(long frequency);
    void getReturns();
    int latency = 100;
    std::map<long, int> BetTimes;
    double maxPositionDifference;
    std::vector<Tick> TickHistory;
    std::vector<int> TickRule;
    //'T': time bars, 'I': tick bars, 'V': volume bars, 'D': dollar bars, 'B': tick imbalance bars
    char CurrentTickChoice = 'T';

    long TimeBarParam = 0;

    long TickBarParam = 0;

    long VolumeBarParam = 0;

    double DollarBarParam = 0;

    int ImbalanceParam = 0;

    std::vector<TradeMessage> currentTick;

    long BetFrequency;

    // BOTTOMLEVEL: THESE FUNCTIONS DEAL WITH THE INTERFACE INFRASTRUCTURE AS WELL AS COMMUNICATION WITH THE ORDERBOOK and MATCHING ENGINE

    // for placed orders, the value of 1 stands for still on queue, 0 stands for traded, and -1 stands for cancelled
    long CurrentShares = 0;
    std::map<int, int> PlacedOrders;
    std::vector<TradeMessage> Trades;
    std::vector<int> CancelledOrders;

    std::map<long, double> TotalPositionData;
    long ReturnsFrequency = 0;
    long currentTime = 0;

    double currentAmount = 0;
    std::vector<double> DollarPositionData;
    std::vector<double> Returns;
    std::map<long, double> BalanceHistory;

    std::map<long, double> AssetPrice;

    double initialAmount = 0;
    std::vector<AddMessage> orderbookBids;

    std::vector<AddMessage> orderbookAsks;

    long lastOrdernumber = -2;

    double getCurrentTime()
    {
        return currentTime;
    }

    virtual void onAdd(const AddMessage &msg);
    virtual void onUpdate(const UpdateMessage &msg);
    virtual void onCancel(const CancelMessage &msg);
    virtual void onTrade(const TradeMessage &msg);

    virtual void onEvent(std::shared_ptr<Event> event);
    void TickGenerate();
    void updateOrder(auto orderNumber, auto newPrice, auto newVolume);

    void cancelOrder(auto orderNumber, auto volume);

    /*__________________________Event Based Tick Bar Generation_____________________________*/

    void setTickChoice(char choice);

    void setTimeBarParam(long param);

    void setTickBarParam(long param);

    void setVolumeBarParam(long param);

    void setDollarBarParam(double param);

    void setImbalanceParam(int param);

    /*__________________________Backtest Statistics_____________________________*/

    double getSharpeRatio();

    double getAverageAUM();

    double getLeverage();

    void setLimitMDPS(double difference);

    int getMDPS();

    void setBetFrequency(long frequency);

    std::vector<int> getBetFrequency();

    long getAverageHoldingPeriod();
};
