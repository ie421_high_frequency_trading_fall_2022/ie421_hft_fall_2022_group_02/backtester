#include "Tick.h"

bool Tick::operator==(const Tick &other) const
{

  return open_time == other.open_time && close_time == other.close_time && v_weighted_average_price == other.v_weighted_average_price && open == other.open && close == other.close && high == other.high && low == other.low && volume_traded == other.volume_traded;
}

std::ostream &operator<<(std::ostream &out, const Tick &tick)
{
  out << "Tick["
      << tick.open_time << " "
      << tick.close_time << " "
      << tick.v_weighted_average_price << " "
      << tick.open << " " << tick.close << " " << tick.high << " " << tick.low << " " << tick.volume_traded << "]";
  return out;
}