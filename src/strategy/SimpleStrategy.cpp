#include "SimpleStrategy.h"
#include <iostream>

void SimpleStrategy::Strategy()
{
    int size = Trades.size();
    TradeMessage last = Trades[size - 1];

    if (last.aggressedSide == B)
    {

        placeBid(last.price, 100);
    }

    else if (last.aggressedSide == S)
    {

        placeAsk(last.price, 100);
    }
}
