
#pragma once

#include <ostream>
#include "types.hpp"
#include "messages.h"
#include <iostream>
#include <vector>

class Tick
{
public:
  long open_time;
  long close_time;
  double v_weighted_average_price;
  double open;
  double close;
  double high;
  double low;
  long volume_traded;

  Tick()
  {
    open_time = 0;
    close_time = 0;
    v_weighted_average_price = 0;
    open = 0;
    close = 0;
    high = 0;
    low = 0;
    volume_traded = 0;
  };

  bool operator==(const Tick &other) const;

  friend std::ostream &operator<<(std::ostream &out, const Tick &tick);

  Tick(long open_time, long close_time, double v_weighted_average_price, double open, double close, double high, double low,
       long volume_traded)
      : open_time(open_time), close_time(close_time), v_weighted_average_price(v_weighted_average_price), open(open),
        close(close), high(high), low(low){};
};
