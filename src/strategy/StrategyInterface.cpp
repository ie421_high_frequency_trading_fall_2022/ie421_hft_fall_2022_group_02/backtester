#include "StrategyInterface.h"
#include <cmath>
void StrategyInterface::onAdd(const AddMessage &msg)
{

    if (msg.side == B)
    {
        orderbookBids.push_back(msg);
    }

    if (msg.side == S)
    {
        orderbookAsks.push_back(msg);
    }
}

void StrategyInterface::onUpdate(const UpdateMessage &msg) {}

void StrategyInterface::onCancel(const CancelMessage &msg) {}

void StrategyInterface::onTrade(const TradeMessage &msg)
{

    // Tick Generation
    currentTick.push_back(msg);

    TickGenerate();

    // Data Structure Generation

    AssetPrice[msg.deliver_time] = msg.price;

    if (msg.orderNumber < 0 || msg.aggressorOrderNumber < 0)
    {
        // std::cout << currentAmount << std::endl;
        //  std::cout << msg.price << std::endl;

        if (msg.orderNumber < 0 && msg.aggressorOrderNumber < 0)
        {

            // std::cout << "THISHAPPENS" << std::endl;
            PlacedOrders[msg.orderNumber] = 0;
            PlacedOrders[msg.aggressorOrderNumber] = 0;
            BalanceHistory[msg.deliver_time] = currentAmount;
        }
        else if (msg.aggressorOrderNumber < 0)
        {

            if (msg.aggressedSide == Side::S)
            {
                // std::cout << "THISHAPPENS" << std::endl;
                // std::cout << "SDLFJLK:DSFJKSDL:" << std::endl;
                CurrentShares += msg.quantity;
                currentAmount = currentAmount - msg.quantity * msg.price;
                BalanceHistory[msg.deliver_time] = currentAmount + CurrentShares * msg.price;
                PlacedOrders[msg.orderNumber] = 0;
                DollarPositionData.push_back(msg.price * msg.quantity);
                BetTimes[msg.deliver_time] = 1;
                // std::cout << "------------------------" << std::endl;
                // std::cout << currentAmount << std::endl;
                // std::cout << msg.deliver_time << "|" << msg.quantity << "|" << msg.price << std::endl;
                // std::cout << CurrentShares << "|" << currentAmount << "|" << currentAmount + CurrentShares * msg.price << std::endl;
            }

            else if (msg.aggressedSide == Side::B)
            {
                // std::cout << "THISHAPPENS" << std::endl;
                // std::cout << "SDLFJLK:DSFJKSDL:" << std::endl;
                CurrentShares -= msg.quantity;
                currentAmount = currentAmount + msg.quantity * msg.price;
                PlacedOrders[msg.orderNumber] = 0;
                // std::cout << msg.deliver_time << "|" << msg.quantity << "|" << msg.price << std::endl;
                // std::cout << CurrentShares << "|" << currentAmount << "|" << currentAmount + CurrentShares * msg.price << std::endl;

                BalanceHistory[msg.deliver_time] = currentAmount + CurrentShares * msg.price;
                BetTimes[msg.deliver_time] = 0;
                // std::cout << "------------------------" << std::endl;
                // std::cout << msg.price * msg.quantity << std::endl;
            }
        }
        else if (msg.orderNumber < 0)
        {

            if (msg.aggressedSide == Side::B)
            {

                // std::cout << "THISHAPPENS" << std::endl;
                CurrentShares += msg.quantity;
                currentAmount = currentAmount - msg.quantity * msg.price;
                PlacedOrders[msg.orderNumber] = 0;
                // std::cout << msg.deliver_time << "|" << msg.quantity << "|" << msg.price << std::endl;
                // std::cout << CurrentShares << "|" << currentAmount << "|" << currentAmount + CurrentShares * msg.price << std::endl;
                BalanceHistory[msg.deliver_time] = currentAmount + CurrentShares * msg.price;
                DollarPositionData.push_back(msg.price * msg.quantity);
                BetTimes[msg.deliver_time] = 1;
                // std::cout << "------------------------" << std::endl;
                // std::cout << currentAmount << std::endl;
            }

            else if (msg.aggressedSide == Side::S)
            {
                // if (msg.deliver_time == 50)
                // {

                //     std::cout << "FOUNDIT" << std::endl;
                //     std::cout << CurrentShares << std::endl;

                //     std::cout << currentAmount << std::endl;
                //     std::cout << msg.quantity << std::endl;
                //     std::cout << msg.price << std::endl;
                // }

                CurrentShares -= msg.quantity;
                currentAmount = currentAmount + msg.quantity * msg.price;
                // std::cout << currentAmount << std::endl;
                PlacedOrders[msg.orderNumber] = 0;
                // std::cout << msg.deliver_time << "|" << msg.quantity << "|" << msg.price << std::endl;
                // std::cout << CurrentShares << "|" << currentAmount << "|" << currentAmount + CurrentShares * msg.price << std::endl;
                BalanceHistory[msg.deliver_time] = currentAmount + CurrentShares * msg.price;
                BetTimes[msg.deliver_time] = 0;
                // std::cout << "------------------------" << std::endl;
                // std::cout << currentAmount << std::endl;
            }
        }
        if (CurrentShares > 0)
        {
            TotalPositionData[msg.deliver_time] = msg.price * CurrentShares;
        }
        Trades.push_back(msg);
    }
    else
    {
        // Strategy call
        Trades.push_back(msg);
        Strategy();
    }
}

void StrategyInterface::Strategy()
{
}

void StrategyInterface::onEvent(std::shared_ptr<Event> event)
{
    currentTime = event->deliver_time + 10;
    switch (event->type)
    {
    case Type::A:
    {
        const AddMessage &msg = *static_pointer_cast<AddMessage>(event);
        this->onAdd(msg);
    }
    break;
    case Type::U:
    {
        const UpdateMessage &msg = *static_pointer_cast<UpdateMessage>(event);
        this->onUpdate(msg);
    }
    break;
    case Type::C:
    {
        const CancelMessage &msg = *static_pointer_cast<CancelMessage>(event);
        this->onCancel(msg);
    }
    break;
    case Type::T:
    {
        const TradeMessage &msg = *static_pointer_cast<TradeMessage>(event);
        this->onTrade(msg);

        // std::cout<<msg.matchNumber<<std::endl;
    }
    break;
    }
}

void StrategyInterface::placeOrder(double price, int quantity, int i)
{
    // std::cout << "Side: " << s << std::endl;
    std::shared_ptr<AddMessage> msg = std::make_shared<AddMessage>();
    msg->orderNumber = lastOrdernumber--;
    msg->deliver_time = currentTime + latency;
    msg->source = SOURCE_STRATEGY;

    if (i == 0)
    {
        msg->side = S;
    }
    else if (i == 1)
    {
        msg->side = B;
    }
    msg->quantity = quantity;
    msg->price = price;
    PlacedOrders[msg->orderNumber] = 1;
    lastOrdernumber = lastOrdernumber--;
    // std::cout << "SIDE: " << side << std::endl;

    push(msg);
}

void StrategyInterface::updateOrder(auto orderNumber, auto newPrice, auto newVolume)
{

    std::shared_ptr<UpdateMessage> msg = std::make_shared<UpdateMessage>();

    msg->orderNumber = orderNumber;
    msg->deliver_time = currentTime;
    msg->source = SOURCE_STRATEGY;
    msg->quantity = newVolume;
    msg->price = newPrice;

    msg->newOrderNumber = lastOrdernumber - 1;

    lastOrdernumber = msg->newOrderNumber;
    _pending_events.push(msg);
};

void StrategyInterface::cancelOrder(auto orderNumber, auto volume)
{

    std::shared_ptr<CancelMessage> msg = std::make_shared<CancelMessage>();

    msg->orderNumber = orderNumber;
    msg->deliver_time = currentTime;
    msg->source = SOURCE_STRATEGY;
    msg->quantity = volume;
    PlacedOrders[msg->orderNumber] = -1;
    _pending_events.push(msg);
};

void StrategyInterface::setInitialAmount(double initial)
{
    initialAmount = initial;
    currentAmount = initialAmount;
}

void StrategyInterface::placeAsk(double price, int shares)
{
    if (CurrentShares > 0)
        placeOrder(price, shares, 0);
}

void StrategyInterface::placeBid(double price, int shares)
{
    if (currentAmount > 0)
        placeOrder(price, shares, 1);
}

std::map<int, int> StrategyInterface::getPlacedOrders()
{

    return PlacedOrders;
}

std::vector<TradeMessage> StrategyInterface::getTrades()
{
    return Trades;
}

double StrategyInterface::getNetProfit()
{

    return currentAmount - initialAmount;
}

void StrategyInterface::SetReturnsFrequency(long frequency)
{
    ReturnsFrequency = frequency;
}
void StrategyInterface::getReturns()
{
    double openBalance = initialAmount;
    long openTime = 0;
    double r = 0;
    for (auto const &x : BalanceHistory)
    {

        if (x.first - openTime >= ReturnsFrequency)
        {

            r = (x.second - openBalance) / openBalance;
            Returns.push_back(r);
            openTime = x.first;
            openBalance = x.second;
        }
    }
}

double StrategyInterface::getSharpeRatio()
{

    double sum;
    for (int i = 0; i < Returns.size(); i++)
    {
        sum += Returns[i];
    }

    double mean = sum / Returns.size();

    double diff = 0;

    for (int i = 0; i < Returns.size(); i++)
    {
        diff += (Returns[i] - mean) * (Returns[i] - mean);
    }

    double std = sqrt(diff / Returns.size());

    return mean / std;
}

double StrategyInterface::getAverageAUM()
{
    int size = TotalPositionData.size();
    double total = 0;

    for (auto const &x : TotalPositionData)
    {

        total += x.second;
    }

    return total / size;
}

double StrategyInterface::getLeverage()
{

    int size = DollarPositionData.size();

    double total = 0;

    for (int i = 0; i < size; i++)
    {
        total += DollarPositionData[i];
    }

    double num = total / size;

    double aum = getAverageAUM();

    return num / aum;
}

void StrategyInterface::setLimitMDPS(double difference)
{
    maxPositionDifference = difference;
}

int StrategyInterface::getMDPS()
{

    double avgAUM = getAverageAUM();

    int size = DollarPositionData.size();

    int total = 0;
    for (int i = 0; i < size; i++)
    {
        if (DollarPositionData[i] > avgAUM)
        {

            total += 1;
        }
    }
    return total;
}

void StrategyInterface::setBetFrequency(long frequency)
{

    BetFrequency = frequency;
}

std::vector<int> StrategyInterface::getBetFrequency()
{

    std::vector<int> BetFrequencies;
    int previous = 1;
    int total = 0;
    long time = 0;
    for (auto const &x : BetTimes)
    {
        // std::cout << x.first << " | " << x.second << std::endl;

        if (((x.first) - time) >= BetFrequency)
        {

            // std::cout << x.first - time << " | "
            //           << " | " << total << std::endl;
            time = x.first;
            BetFrequencies.push_back(total);
            total = 1;
        }
        else if (x.second != previous)
        {

            total += 1;
        }
        previous = x.second;
    }
    return BetFrequencies;
}

long StrategyInterface::getAverageHoldingPeriod()
{

    std::vector<long> holdingperiods;
    int previous = 1;

    int total = 0;

    long timeDifference = 0;

    long time = 0;
    for (auto const &x : BetTimes)
    {

        if (time == 0)
        {
            time = x.first;
            continue;
        }

        if (x.second == previous)
        {
            total += 1;
        }
        else
        {
            timeDifference = x.first - time;
            holdingperiods.push_back(timeDifference);
            time = x.first;
        }
        previous = x.second;
    }

    double s = 0;
    for (int i = 0; i < holdingperiods.size(); i++)
    {
        s += holdingperiods[i];
    }

    return s / holdingperiods.size();
}

void StrategyInterface::setTickChoice(char choice)
{
    CurrentTickChoice = choice;
}

void StrategyInterface::setTimeBarParam(long param)
{

    TimeBarParam = param;
}

void StrategyInterface::setTickBarParam(long param)
{

    TickBarParam = param;
}

void StrategyInterface::setVolumeBarParam(long param)
{

    VolumeBarParam = param;
}

void StrategyInterface::setDollarBarParam(double param)
{

    DollarBarParam = param;
}

void StrategyInterface::setImbalanceParam(int param)
{

    ImbalanceParam = param;
}

void StrategyInterface::TickGenerate()
{
    if (CurrentTickChoice == 'T')
    {
        long initialTime = currentTick[0].deliver_time;
        long finalTime = currentTick[currentTick.size() - 1].deliver_time;

        if (finalTime - initialTime > TimeBarParam)
        {

            Tick t;
            t.open_time = currentTick.front().deliver_time;
            t.close_time = currentTick.back().deliver_time;
            t.open = currentTick.front().price;
            t.close = currentTick.back().price;

            long total = currentTick[0].quantity, min_val = currentTick[0].price, max_val = currentTick[0].price;

            for (int i = 1; i < currentTick.size(); i++)
            {

                total += currentTick[i].quantity;
                if (currentTick[i].price > max_val)
                {
                    max_val = currentTick[i].price;
                }
                if (currentTick[i].price < min_val)
                {
                    min_val = currentTick[i].price;
                }
            }
            t.low = min_val;
            t.high = max_val;
            t.volume_traded = total;

            TickHistory.push_back(t);
            currentTick.clear();
        }
    }
    if (CurrentTickChoice == 'I')
    {
        if (currentTick.size() > TickBarParam)
        {

            Tick t;
            t.open_time = currentTick.front().deliver_time;
            t.close_time = currentTick.back().deliver_time;
            t.open = currentTick.front().price;
            t.close = currentTick.back().price;

            long total = currentTick[0].quantity, min_val = currentTick[0].price, max_val = currentTick[0].price;

            for (int i = 1; i < currentTick.size(); i++)
            {

                total += currentTick[i].quantity;
                if (currentTick[i].price > max_val)
                {
                    max_val = currentTick[i].price;
                }
                if (currentTick[i].price < min_val)
                {
                    min_val = currentTick[i].price;
                }
            }
            t.low = min_val;
            t.high = max_val;
            t.volume_traded = total;

            TickHistory.push_back(t);
            currentTick.clear();
        }
    }
    if (CurrentTickChoice == 'V')
    {
        long volume = 0;
        for (int i = 0; i < currentTick.size(); i++)
        {
            volume += currentTick[i].quantity;
        }
        if (volume > VolumeBarParam)
        {

            Tick t;
            t.open_time = currentTick.front().deliver_time;
            t.close_time = currentTick.back().deliver_time;
            t.open = currentTick.front().price;
            t.close = currentTick.back().price;

            long total = currentTick[0].quantity, min_val = currentTick[0].price, max_val = currentTick[0].price;

            for (int i = 1; i < currentTick.size(); i++)
            {

                total += currentTick[i].quantity;
                if (currentTick[i].price > max_val)
                {
                    max_val = currentTick[i].price;
                }
                if (currentTick[i].price < min_val)
                {
                    min_val = currentTick[i].price;
                }
            }
            t.low = min_val;
            t.high = max_val;
            t.volume_traded = total;

            TickHistory.push_back(t);
            currentTick.clear();
        }
    }
    if (CurrentTickChoice == 'D')
    {

        double dollar = 0;
        for (int i = 0; i < currentTick.size(); i++)
        {
            dollar += currentTick[i].quantity * currentTick[i].price;
        }

        if (dollar > DollarBarParam)
        {

            Tick t;
            t.open_time = currentTick.front().deliver_time;
            t.close_time = currentTick.back().deliver_time;
            t.open = currentTick.front().price;
            t.close = currentTick.back().price;

            long total = currentTick[0].quantity, min_val = currentTick[0].price, max_val = currentTick[0].price;

            for (int i = 1; i < currentTick.size(); i++)
            {

                total += currentTick[i].quantity;
                if (currentTick[i].price > max_val)
                {
                    max_val = currentTick[i].price;
                }
                if (currentTick[i].price < min_val)
                {
                    min_val = currentTick[i].price;
                }
            }
            t.low = min_val;
            t.high = max_val;
            t.volume_traded = total;

            TickHistory.push_back(t);
            currentTick.clear();
        }
    }
    if (CurrentTickChoice == 'B')
    {
        TickRule.clear();
        int tickSum = 0;
        for (int i = 1; i < currentTick.size(); i++)
        {

            double change = currentTick[i].price - currentTick[i - 1].price;

            tickSum += (abs(change) / change);
        }

        if (tickSum > ImbalanceParam)
        {

            Tick t;
            t.open_time = currentTick.front().deliver_time;
            t.close_time = currentTick.back().deliver_time;
            t.open = currentTick.front().price;
            t.close = currentTick.back().price;

            long total = currentTick[0].quantity, min_val = currentTick[0].price, max_val = currentTick[0].price;

            for (int i = 1; i < currentTick.size(); i++)
            {

                total += currentTick[i].quantity;
                if (currentTick[i].price > max_val)
                {
                    max_val = currentTick[i].price;
                }
                if (currentTick[i].price < min_val)
                {
                    min_val = currentTick[i].price;
                }
            }
            t.low = min_val;
            t.high = max_val;
            t.volume_traded = total;

            TickHistory.push_back(t);
            currentTick.clear();
        }
    }
}