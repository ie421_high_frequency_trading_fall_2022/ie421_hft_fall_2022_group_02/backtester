#pragma once

enum Type : unsigned char { A, U, C, T };

struct Event
{
    char type;
    long register_time;
    long deliver_time;
    void* payload;

    void copy(const Event* other) {
        type = other->type;
        register_time = other->register_time;
        deliver_time = other->deliver_time;
    }
};
