#pragma once

#include <thread>
#include <condition_variable>
#include <mutex>
#include <iostream>
/** Feed Replay should extend this class.
 * 
*/
class TimeManager
{
private:
    std::condition_variable _cv;
    std::mutex _mtx;
    long _fetched_time;
    

public:
    long _snapshot_time;
    bool session_ended = false;
    TimeManager(/* args */);
    TimeManager(long snap, long fetch);

    void SetSnapshotTime(long time);
    void ProceedSnapshot();
    void ProceedFetchToTime(long timestamp);
    
};
