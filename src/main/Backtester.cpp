#include "Backtester.h"
#include "messages.h"


Backtester::Backtester(std::shared_ptr<TimeManager> mgr, const std::string& csv_out, const std::string& db_out) : _time_manager(mgr), writer(db_out) {
    _should_record = (csv_out != "");
    if (_should_record) {
        _events.open(csv_out, std::ofstream::out | std::ofstream::trunc);
        _events << "timestamp,source,match,aggressed,aggressor,aggressedside,quantity,price" << std::endl;
    }
}

Backtester::~Backtester() {
    if (_should_record) {
        _events.close();
    }
}

void Backtester::_proceed() {
    if (strategy->peek() < _time_manager->_snapshot_time) {
        
        std::cerr << "warn" << strategy->peek()<< std::endl;
        std::cerr << "snap" << _time_manager->_snapshot_time<< std::endl;
        strategy->pop();
        return;
    }
    if (exchange->peek() < _time_manager->_snapshot_time) {
        exchange->pop();
        std::cerr << "warn" << std::endl;
        return;
    }
    if (exchange->peek() < strategy->peek()) {
        _time_manager->SetSnapshotTime(exchange->peek());
        if (_should_record) _record(exchange->get());
        strategy->onEvent(exchange->get());
        writer.record(exchange->get());
        exchange->pop();
    } else {
        if (strategy->peek() == __LONG_MAX__) return;
        _time_manager->SetSnapshotTime(strategy->peek());
        exchange->onEvent(strategy->get());
        strategy->pop();
    }
    if (_counter++ % 10000 == 0) {
        std::cerr << "Event Count: " << _counter << std::endl;
        std::cerr << "Timestamp: " << _time_manager->_snapshot_time << std::endl;
    }
}

void Backtester::_record(const std::shared_ptr<Event>& event) {
    if (event->type != Type::T) return;
    decltype(auto) trade = *static_pointer_cast<TradeMessage>(event);
    _events << trade.deliver_time << ","
            << trade.source << ","
            << trade.matchNumber << ","
            << trade.orderNumber << ","
            << trade.aggressorOrderNumber << ","
            << (int) trade.aggressedSide << ","
            << trade.quantity << ","
            << trade.price << std::endl;
}

int Backtester::run() {
    while (!_time_manager->session_ended) {
        _time_manager->ProceedSnapshot();
        _proceed();
    }
    while (strategy->peek() < __LONG_MAX__ || exchange->peek() < __LONG_MAX__) {
        _time_manager->ProceedSnapshot();
        _proceed();
    }

    std::cout << "Main complete!" << std::endl;
    return 0;
}

void Backtester::registerExchange(std::shared_ptr<EventListener> exchange) {
    this->exchange = exchange;
}

void Backtester::registerStrategy(std::shared_ptr<EventListener> strategy) {
    this->strategy = strategy;
}

