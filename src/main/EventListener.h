#pragma once
#include <queue>
#include <memory>
#include <mutex>
#include "Event.h"
#include "types.hpp"

class EventListener
{
protected:
    EventQueue _pending_events;
    std::mutex _mtx;
public:
    EventListener() {};
    virtual ~EventListener();
    virtual void onEvent(std::shared_ptr<Event> evt) {};
    long peek();
    virtual std::shared_ptr<Event> get();
    void pop();
    void push(std::shared_ptr<Event> evt);
};
