# Main Component

Main component is a layer that handles the interaction between data fetching, order book and user strategy. It also maintains syncronization between fetcher thread and ob/strategy thread.

## Usage

### Backtester
- Each backtester maintains a TimeManager, a strategy and a order book. 
- Backtester should run on thread with `run()` function.
- Backtester proceeds in time and determine the next event to be triggered. 
- Backtester will be blocked on insufficient events.
### TimeManager
- Each time manager belongs to a backtester and a data fetcher (See `tests/src/MockDataLoader` ).
- Dataloader has blocking functions that should be called correctly to synchronize. 
- Dataloader will release blocked backtester on fetcher calls showing more data is loaded. 
### EventListener
- Abstraction layer for communication and time-keeping between components.