#include "TimeManager.h"


TimeManager::TimeManager(long snap, long fetch): _snapshot_time(snap), _fetched_time(fetch) { }

/** Called by main thread running strategy and order book.
 * 
*/
void TimeManager::ProceedSnapshot() {
    
    std::unique_lock lk(_mtx);
    _cv.wait(lk, [this] {return this->_snapshot_time < this->_fetched_time || session_ended; });
    // Do work.

    lk.unlock();

    // Or here.
    
}

/** Called by data loader thread.
 * 
*/
void TimeManager::ProceedFetchToTime(long timestamp) {
    std::unique_lock lk(_mtx);

    _fetched_time = timestamp;

    lk.unlock();
    _cv.notify_one();

}

void TimeManager::SetSnapshotTime(long time) {
    _snapshot_time = time;
}
