#include "EventListener.h"
#include <exception>

#define EOD_TIME 55000000000000

EventListener::~EventListener() {}
long EventListener::peek() {
    std::unique_lock lk(_mtx);
    auto time = (_pending_events.empty()) ? __LONG_MAX__ : _pending_events.top()->deliver_time;
    lk.unlock();
    return time;
}

void EventListener::pop() {
    std::unique_lock lk(_mtx);
    _pending_events.pop();
    lk.unlock();
}

std::shared_ptr<Event> EventListener::get() {
    std::unique_lock lk(_mtx);
    if (_pending_events.empty()) {
      lk.unlock();
      std::__throw_domain_error("No events.");
    }

    auto evt = _pending_events.top();
    lk.unlock();
    return evt;
}

void EventListener::push(std::shared_ptr<Event> evt) {
    std::unique_lock lk(_mtx);
    if (evt->deliver_time < EOD_TIME)
    _pending_events.push(evt);
    lk.unlock();
}
