#pragma once

#include "TimeManager.h"
#include "EventListener.h"
#include "SqlDump.h"
#include <fstream>
#include <memory>
#include <queue>
#include <iostream>
#include <string>

class Backtester
{
private:
    std::shared_ptr<TimeManager> _time_manager;
    std::shared_ptr<EventListener> exchange;
    std::shared_ptr<EventListener> strategy;
    std::priority_queue<Event> pending_actions;
    bool _should_record;
    std::ofstream _events;
    SqlDump writer;
    int _counter = 0;
    void _proceed();
    void _record(const std::shared_ptr<Event>& event);

public:
    Backtester(std::shared_ptr<TimeManager> mgr, const std::string& csv_out = "", const std::string& db_out = "./dev0.db");
    ~Backtester();
    int run();
    void registerExchange(std::shared_ptr<EventListener> exchange);
    void registerStrategy(std::shared_ptr<EventListener> strategy);

};
