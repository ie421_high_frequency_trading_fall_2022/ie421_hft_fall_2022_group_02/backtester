#pragma once

#include "EventListener.h"
#include <iostream>
class EmptyStrategy : public EventListener
{
private:
    /* data */
    char typeText[4] = {'A', 'U', 'C', 'T'};
    int counter = 0;
    int counts[4]{ 0, 0, 0, 0};
public:
    EmptyStrategy() {};
    ~EmptyStrategy() {};
    
    void onEvent(std::shared_ptr<Event> evt) override;
    
};

void EmptyStrategy::onEvent(std::shared_ptr<Event> evt) {
    counter ++;
    ++counts[(int) evt->type];
    if (counter % 50000 == 0) {
        std::cout << "Event Number: " << counter << " " << typeText[evt->type] << " at time: " << evt->deliver_time << std::endl;
        std::cout << "Counts: A: " << counts[0] << " U: " << counts[1] << " C: " << counts[2] << " T: " << counts[3] << std::endl;
        if (evt->type == Type::A) {
            auto msg = std::static_pointer_cast<AddMessage>(evt);
            std::cout << "P: "<< msg->price << " Q: " << msg->quantity << std::endl;
        } 
        if (evt->type == Type::U) {
            auto msg = std::static_pointer_cast<UpdateMessage>(evt);
            std::cout << "P: "<< msg->price << " Q: " << msg->quantity << std::endl;
        }
        if (evt->type == Type::C) {
            auto msg = std::static_pointer_cast<CancelMessage>(evt);
            std::cout << "Cancel: "<< msg->orderNumber << std::endl;
        }
        if (evt->type == Type::T) {
            auto msg = std::static_pointer_cast<TradeMessage>(evt);
            std::cout << "P: "<< msg->price << " Q: " << msg->quantity << std::endl;
        }
    }
}
