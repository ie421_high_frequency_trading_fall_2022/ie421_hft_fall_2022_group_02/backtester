#pragma once

class Uid {
public:
  static long get() {
    return _x++;
  }

private:
  static long _x;
};

long Uid::_x = 0;

