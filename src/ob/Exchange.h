#pragma once
#include "IExchange.h"

#include <unordered_map>
#include "MatchingEngine.h"
#include "types.hpp"

class Exchange : public IExchange {
public:
  Exchange(){};
  ~Exchange(){};

  void onAdd(const AddMessage& msg) override;
  void onUpdate(const UpdateMessage& msg) override;
  void onCancel(const CancelMessage& msg) override;
  void onTrade(const TradeMessage& msg) override;

  void onEvent(std::shared_ptr<Event> event) override;

private:
  std::unordered_map<long, MatchingEngine> _engines;

  MatchingEngine& getOrCreate(long stockLocate);
};

