#pragma once
#include "ISnapshot.h"

class Snapshot : public ISnapshot {
public:
  Snapshot(){}
  Snapshot(MarketSidePtr bids, MarketSidePtr asks)
      : _bids(bids), _asks(asks) {}

  const std::map<double, PriceLevel>& bids() override { return *_bids; }
  const std::map<double, PriceLevel>& asks() override { return *_asks; }

private:
  MarketSidePtr _bids;
  MarketSidePtr _asks;
};

