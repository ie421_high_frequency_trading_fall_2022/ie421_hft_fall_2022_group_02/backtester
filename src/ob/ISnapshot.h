#pragma once
#include "types.hpp"

class ISnapshot {
public:
  ISnapshot(){};
  virtual ~ISnapshot() = default;

  virtual const std::map<double, PriceLevel>& bids() = 0;
  virtual const std::map<double, PriceLevel>& asks() = 0;
};

