# Backtester Exchange

## Implementation
Like an onion, an `Exchange` contains a mapping of `MatchingEngine`s, which each contain an `OrderBook` for the specific `stockLocate`.
![implementation](/assets/exchange_implementation.png)

## Lifecycle
When an `event` comes in,
1. the exchange checks the type and gets casted to the correct `Message` type, and 
1. gets dispatched to the correct interface function for the `MatchingEngine` with the corresponding `stockLocate`. 
1. The `engine` forwards the `message` to the `OrderBook` which 
1. updates the bids and asks (for less lookups and faster accesses). 
1. If any resulting events (e.g. trade was performed) was emitted, it will be added to the `pending_events` queue of the `Exchange`, which is a priority queue by `deliver_time` of outgoing `Event`s.  
![lifecycle](/assets/exchange_lifecycle.png)

## Matching
We implement a custom simple matching algorithm to balance correctness and time required for development. In the usual setting (to generate order books from market data), no real matching is required - when an `AddMessage` is received, an order is added, etc., and when a `TradeMessage` is received, the resting `Order` has its quantity reduced. However, we need to contend with `Message`s from both the historical data, and from the backtested strategy. The most optimal solution will be for strategy orders to shadow lower priority historical orders, but this is complicated (it can be a one strategy to many historical or vice versa, and can change due to `UpdateMessage`s). Given the limited time available, we decide to put this optimal implementation lower down the list. The current implementation is:
- Priority is by best price then earliest time `Add`ed.
- Historical `TradeMessage`s get converted to `AddMessage`s. This works because if a new order can be matched, it will be matched immediately, and a `TradeMessage` will be sent instead of (and not in addition to) an `AddMessage`. So in the absence of strategy messages, this will just replay historical data.
- Matches between historical and strategy orders occur destructively - both orders are removed from the book. We debated this implementation for quite a bit, but this seemed like the best balance between correctness for the book and for the strategy.
