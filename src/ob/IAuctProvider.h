#pragma once

#include <memory>
#include "IAuctConsumer.h"

// TODO can probably be depreciated
class IAuctProvider {
public:
  IAuctProvider(){};
  virtual ~IAuctProvider() = default;

  virtual void registerConsumer(std::shared_ptr<IAuctConsumer> consumer) = 0;
  virtual void next() = 0;
};

