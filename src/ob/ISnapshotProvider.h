#pragma once
#include "Snapshot.hpp"

class ISnapshotProvider {
public:
  ISnapshotProvider(){};
  virtual ~ISnapshotProvider() = default;

  virtual Snapshot snapshot() = 0;
};

