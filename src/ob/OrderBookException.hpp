#pragma once
#include <string>

class OrderBookException : public std::exception {
public:
  OrderBookException(std::string msg) : _msg(msg) {}
  const char* what() const noexcept override { return _msg.c_str(); }

  static void assertThrows(bool assertion, std::string msg) {
    if (!assertion) throw OrderBookException(msg);
  }

  static std::string badNumber(long number) {
    return "Bad order number " + number;
  }

  static std::string badCancel(long number) {
    return "Bad cancel " + number;
  }

private:
  std::string _msg;
};

