#include "Order.h"

bool Order::operator==(const Order& other) const {
  return number == other.number
      && side == other.side
      && quantity == other.quantity
      && price == other.price;
}

std::ostream& operator<<(std::ostream& out, const Order& order) {
  out << "Order["
      << order.number << " "
      << order.side << " "
      << order.quantity << " "
      << order.price << "]";
  return out;
}

