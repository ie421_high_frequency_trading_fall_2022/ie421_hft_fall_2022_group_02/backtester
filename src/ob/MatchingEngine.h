#pragma once
#include "IMatchingEngine.h"

#include "EventListener.h"
#include "OrderBook.h"
#include "types.hpp"

/* This is actually a bit of a misnomer, this only
 * manages one OrderBook.
 */
class MatchingEngine : public IMatchingEngine {
public:
  MatchingEngine(EventListener* listener);

  void onAdd(const AddMessage& msg) override;
  void onUpdate(const UpdateMessage& msg) override;
  void onCancel(const CancelMessage& msg) override;
  void onTrade(const TradeMessage& msg) override;

  Snapshot snapshot() override;

private:
  int _locate;
  OrderBook _book;

  EventListener* _listener;

  double bestBid();
  double bestAsk();
};

