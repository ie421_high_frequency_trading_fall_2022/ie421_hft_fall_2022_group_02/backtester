#include "MatchingEngine.h"

MatchingEngine::MatchingEngine(EventListener* listener) {
  _listener = listener;
  _book = OrderBook(_listener);
}

void MatchingEngine::onAdd(const AddMessage& msg) {
  AddMessage remaining;
  remaining = _book.trade(msg);
  if (remaining.quantity > 0) _book.onAdd(remaining);
}

void MatchingEngine::onUpdate(const UpdateMessage& msg) {
  _book.onUpdate(msg);
}

void MatchingEngine::onCancel(const CancelMessage& msg) {
  _book.onCancel(msg);
}

void MatchingEngine::onTrade(const TradeMessage& msg) {
  AddMessage add;
  // TODO fix this jank
  ((AuctMessage*) &add)->copy((AuctMessage*) &msg);
  add.quantity = msg.quantity;
  add.price = msg.price;
  add.side = (msg.aggressedSide == Side::B) ? Side::S : Side::B;

  onAdd(add);
}

Snapshot MatchingEngine::snapshot() {
  return _book.snapshot();
}

