#pragma once
#include "IAuctConsumer.h"
#include "ISnapshotProvider.h"

#include <unordered_map>
#include "EventListener.h"
#include "Order.h"
#include "types.hpp"

class MatchingEngine;

class OrderBook : public IAuctConsumer, public ISnapshotProvider {
public:
  OrderBook(EventListener* listener);

  void onAdd(const AddMessage& msg) override;
  void onUpdate(const UpdateMessage& msg) override;
  void onCancel(const CancelMessage& msg) override;
  void onTrade(const TradeMessage& msg) override;

  Snapshot snapshot() override;

  double bestBid();
  double bestAsk();

protected:
  OrderBook() : OrderBook(nullptr) {};
  AddMessage trade(const AddMessage& msg);
private:
  friend class MatchingEngine;
  EventListener* _listener;

  MarketSidePtr _bids;
  MarketSidePtr _asks;

  int _locate;
  std::unordered_map<long, std::shared_ptr<Order>> _orders;

  MarketSidePtr& getSide(const std::shared_ptr<Order>& order);

  void eraseOrderReferences(const std::shared_ptr<Order>& order);

  void tradeAtPriceLevelDestructive(double price, PriceLevel& level, AddMessage& msg);
};

