#include "OrderBook.h"

#include <map>
#include <ranges>
#include "OrderBookException.hpp"
#include "Uid.h"

// TODO possibly use raw ptr
OrderBook::OrderBook(EventListener* listener) {
  _bids = std::make_shared<std::map<double, PriceLevel>>();
  _asks = std::make_shared<std::map<double, PriceLevel>>();

  _listener = listener;
}

void OrderBook::onAdd(const AddMessage& msg) {
  if (_orders.count(msg.orderNumber)) {
    printf("Repeated order number %ld, same side? %d\n", msg.orderNumber, _orders.at(msg.orderNumber)->side == msg.side);
    return;
  }
  _listener->push(std::make_shared<AddMessage>(msg));

  // TODO this should probably be a unique ptr actually
  std::shared_ptr<Order> order = std::make_shared<Order>(msg.orderNumber, msg.side, msg.quantity, msg.price);
  _orders.emplace(msg.orderNumber, order);
  
  MarketSidePtr& side = getSide(order);
  side->try_emplace(order->price);
  side->at(order->price).emplace_back(order);
  order->levelLoc = std::prev(side->at(order->price).end());
};

void OrderBook::onUpdate(const UpdateMessage& msg) {
  if (!_orders.contains(msg.orderNumber)) return;

  _listener->push(std::make_shared<UpdateMessage>(msg));

  auto entry = _orders.extract(msg.orderNumber);

  if (msg.quantity == 0) {
    eraseOrderReferences(entry.mapped());
    return;
  }

  entry.mapped()->number = msg.newOrderNumber;
  entry.mapped()->quantity = msg.quantity;
  entry.key() = entry.mapped()->number;

  if (msg.price != entry.mapped()->price) {
    MarketSidePtr& side = getSide(entry.mapped());
    side->at(entry.mapped()->price).erase(entry.mapped()->levelLoc);
    entry.mapped()->price = msg.price;
    side->try_emplace(entry.mapped()->price);
    side->at(entry.mapped()->price).emplace_back(entry.mapped());
    entry.mapped()->levelLoc = std::prev(side->at(entry.mapped()->price).end());
  }

  _orders.insert(std::move(entry));
}

void OrderBook::onCancel(const CancelMessage& msg) {
  if (!_orders.contains(msg.orderNumber)) return;

  std::shared_ptr<Order>& order = _orders.at(msg.orderNumber);
  if (order->quantity <= msg.quantity || msg.quantity == CANCEL_ALL) {
    eraseOrderReferences(order);
    _orders.erase(order->number);
  } else {
    order->quantity -= msg.quantity;
  }

  _listener->push(std::make_shared<CancelMessage>(msg));
}

void OrderBook::onTrade(const TradeMessage& msg) {
  // TODO The generated CancelMessage probably should not be published  Bohan: Do not remove, Dumper rely on this information to record order history.
  _listener->push(std::make_shared<TradeMessage>(msg));
  // For off book trades, do nothing for now
  if (msg.orderNumber == OFF_BOOK) return;

  CancelMessage cancel;
  cancel.copy(&msg);
  cancel.type = Type::C;
  cancel.quantity = msg.quantity;
  onCancel(cancel);
}

Snapshot OrderBook::snapshot() {
  return Snapshot(_bids, _asks);
}

double OrderBook::bestBid() {
  // TODO Magic numbers, might be easier to do 1 << 6 or smth
  return (_bids->empty()) ? -1000000 : _bids->rbegin()->first;
}

double OrderBook::bestAsk() {
  return (_asks->empty()) ? 1000000 : _asks->begin()->first;
}

AddMessage OrderBook::trade(const AddMessage& msg) {
  AddMessage remaining;
  remaining.copy(msg);

  while (remaining.quantity > 0 && ((msg.side == Side::B && !_asks->empty()) || (msg.side == Side::S && !_bids->empty()))) {
    double price = (msg.side == Side::B) ? _asks->begin()->first : _bids->rbegin()->first;
    PriceLevel& level = (msg.side == Side::B) ? _asks->begin()->second : _bids->rbegin()->second;
    if ((msg.side == Side::B && price > msg.price) || (msg.side == Side::S && price < msg.price)) break;
    if (level.empty()) {
      if (msg.side == Side::B) {
        _asks->erase(price);
      } else {
        _bids->erase(price);
      }
      continue;
    }
    tradeAtPriceLevelDestructive(price, level, remaining);
  }

  return remaining;
}

void OrderBook::tradeAtPriceLevelDestructive(double price, PriceLevel& level, AddMessage& msg) {
  TradeMessage trade;
  trade.copy(&msg);
  trade.type = Type::T;
  trade.source = msg.source;
  trade.price = price;
  trade.aggressorOrderNumber = msg.orderNumber;
  trade.aggressedSide = (msg.side == Side::B) ? Side::S : Side::B;

  bool shouldTerminate = false;
  while (msg.quantity > 0 && !shouldTerminate) {
    trade.matchNumber = Uid::get();
    auto& orderPtr = level.front();
    trade.orderNumber = orderPtr->number;
    trade.quantity = std::min(msg.quantity, orderPtr->quantity);
    shouldTerminate = trade.quantity == orderPtr->quantity && level.size() == 1;
    msg.quantity -= trade.quantity;
    onTrade(trade);
  }
} 

MarketSidePtr& OrderBook::getSide(const std::shared_ptr<Order>& order) {
  return (order->side == Side::B) ? _bids : _asks;
}

void OrderBook::eraseOrderReferences(const std::shared_ptr<Order>& order) {
    PriceLevel& level = getSide(order)->at(order->price);
    if (level.size() == 1) {
      getSide(order)->erase(order->price);
    } else {
      level.erase(order->levelLoc);
    }
}

