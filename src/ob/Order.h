#pragma once

#include <ostream>
#include "types.hpp"
#include "messages.h"

class Order {
public:
  long    number;
  Side    side;
  long    quantity;
  double  price;

  PriceLevel::iterator levelLoc;

  Order(long number, Side side, long quantity, double price)
      : number(number), side(side), quantity(quantity), price(price) {}

  bool operator==(const Order& other) const;

  friend std::ostream& operator<<(std::ostream& out, const Order& order);
};

