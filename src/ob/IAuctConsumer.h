#pragma once

#include "messages.h"

class IAuctConsumer {
public:
  IAuctConsumer(){};
  virtual ~IAuctConsumer() = default;

  // TODO maybe we want to use forwarding and enable mangling
  // for efficiency and speed
  virtual void onAdd(const AddMessage& msg) = 0;
  virtual void onUpdate(const UpdateMessage& msg) = 0;
  virtual void onCancel(const CancelMessage& msg) = 0;
  virtual void onTrade(const TradeMessage& msg) = 0;
};

