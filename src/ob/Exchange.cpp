#include "Exchange.h"

void Exchange::onEvent(std::shared_ptr<Event> event) {
  switch (event->type) {
  case Type::A:
    {
      const AddMessage& msg = *static_pointer_cast<AddMessage>(event);
      getOrCreate(msg.stockLocate).onAdd(msg);
    }
    break;
  case Type::U:
    {
      const UpdateMessage& msg = *static_pointer_cast<UpdateMessage>(event);
      _engines.at(msg.stockLocate).onUpdate(msg);
    }
    break;
  case Type::C:
    {
      const CancelMessage& msg = *static_pointer_cast<CancelMessage>(event);
      _engines.at(msg.stockLocate).onCancel(msg);
    }
    break;
  case Type::T:
    {
      const TradeMessage& msg = *static_pointer_cast<TradeMessage>(event);
      getOrCreate(msg.stockLocate).onTrade(msg);
    }
    break;
  }
}

void Exchange::onAdd(const AddMessage& msg) {
  getOrCreate(msg.stockLocate).onAdd(msg);
}

void Exchange::onUpdate(const UpdateMessage& msg) {
  _engines.at(msg.stockLocate).onUpdate(msg);
}

void Exchange::onCancel(const CancelMessage& msg) {
  _engines.at(msg.stockLocate).onCancel(msg);
}

void Exchange::onTrade(const TradeMessage& msg) {
  getOrCreate(msg.stockLocate).onTrade(msg);
}

MatchingEngine& Exchange::getOrCreate(long stockLocate) {
  if (!_engines.contains(stockLocate))
      _engines.emplace(std::piecewise_construct,
          std::forward_as_tuple(stockLocate),
          std::forward_as_tuple(this));

  return _engines.at(stockLocate);
}

