#pragma once
#include "IAuctConsumer.h"
#include "Snapshot.hpp"

/* Reconciles orders from historical data with strategy. Historical trade orders
 * are converted to add orders, and matching is done internally.
 *
 * To process a message, fill in the correct message struct with the appropriate
 * source, and call the appropriate on<Action> callback.
 */
class IMatchingEngine : public IAuctConsumer {
public:
  IMatchingEngine(){};
  virtual ~IMatchingEngine() = default;

  virtual Snapshot snapshot() = 0;
};

