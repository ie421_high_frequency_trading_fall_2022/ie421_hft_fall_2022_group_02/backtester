#pragma once

#include <algorithm>
#include <memory>
#include "Event.h"

#define CANCEL_ALL -1
#define OFF_BOOK -1
#define SOURCE_HISTORICAL 'H'
#define SOURCE_STRATEGY   'S'

enum Side : unsigned char { B, S };

struct AuctMessage : public Event {
  int   stockLocate;
  long  orderNumber;
  long  sequenceNumber;
  // Note timestamp is deprecated!
  long  timestamp;
  char  source;

  void copy(const AuctMessage* other) {
    Event::copy((const Event*) other);

    stockLocate = other->stockLocate;
    orderNumber = other->orderNumber;
    sequenceNumber = other->sequenceNumber;
    timestamp = other->timestamp;
    source = other->source;
  }

  bool operator==(const AuctMessage& other) const {
    return type == other.type
        && stockLocate == other.stockLocate
        && orderNumber == other.orderNumber
        && sequenceNumber == other.sequenceNumber
        && timestamp == other.timestamp
        && source == other.source;
  }
};

struct AddMessage : public AuctMessage {
  char    symbol[8];
  Side    side;
  long    quantity;
  double  price;

  AddMessage() {
    type = Type::A;
  }

  void copy(const AddMessage& other) {
    AuctMessage::copy(&other);
    std::copy(other.symbol, other.symbol + 8, symbol);
    side = other.side;
    quantity = other.quantity;
    price = other.price;
  }

  bool operator==(const AddMessage& other) const {
    return AuctMessage::operator==(other)
      // TODO not comparing symbol, maybe it should be dropped
        && side == other.side
        && quantity == other.quantity
        && price == other.price;
  }
};

struct UpdateMessage : public AuctMessage {
  long    newOrderNumber;
  long    quantity;
  double  price;
  Side    side;

  UpdateMessage() {
    type = Type::U;
  }
};

struct CancelMessage : public AuctMessage {
  long  quantity;
  double price;
  Side    side;
  CancelMessage() {
    type = Type::C;
  }
};

struct TradeMessage : public AuctMessage {
  char    symbol[8];
  Side    aggressedSide;
  long    matchNumber;
  long    aggressorOrderNumber;
  long    quantity;
  double  price;

  TradeMessage() {
    type = Type::T;
  }
};

class AuctCompare {
public:
  bool operator()(const std::shared_ptr<AuctMessage>& a, const std::shared_ptr<AuctMessage>& b) {
    if (a->deliver_time != b->deliver_time) return a->deliver_time > b->deliver_time;
    if (a->stockLocate != b->stockLocate) return a->stockLocate > b->stockLocate;
    return a->orderNumber > b->orderNumber;
  }
};

