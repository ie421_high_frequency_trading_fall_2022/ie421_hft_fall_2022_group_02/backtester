#include <chrono>
#include <iostream>
#include <string.h>
#include <thread>
#include "CsvLoader.h"
#include "Exchange.h"
#include "EmptyStrategy.hpp"
#include "TimeManager.h"
#include "Backtester.h"
#include "SqlDump.h"
#include "TestStrategy.h"
#include "StrategyInterface.h"
#include "SimpleStrategy.h"

static std::pair<std::string, std::shared_ptr<EventListener>> strategy_array[] = {
    {"Counter", std::make_shared<EmptyStrategy>()},
    {"Test", std::make_shared<TestStrategy>()},
    {"Simple Trade on Historical Trade", std::make_shared<SimpleStrategy>()}};

int main(int argc, char *argv[])
{
  std::string csv_output = "/vagrant/data/events.csv";
  std::string db_output = "/vagrant/data/dev.db";
  std::string input = "/vagrant/data/07302019.NASDAQ_ITCH50_AAPL_message.csv";
  int idx = 2;
  if (argc > 1)
  {
    if (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0)
    {
      std::cout << "backtester [(int) strategy index] [(path) input historical CSV] [(path) output trade CSV] [(path) output orders DB]" << std::endl;
      return 0;
    }

    idx = std::stoi(std::string(argv[1]));
  }
  if (argc > 2)
    input = std::string(argv[2]);
  if (argc > 3)
    csv_output = std::string(argv[3]);
  if (argc > 4)
    db_output = std::string(argv[4]);

  auto ex = std::make_shared<Exchange>();
  auto st = strategy_array[idx];
  auto tm = std::make_shared<TimeManager>(0, -1);
  Backtester bk = Backtester(tm, csv_output, db_output);

  bk.registerExchange(ex);
  bk.registerStrategy(st.second);

  double InitialAmount = 0;
  int ReturnsFrequency = 0;
  double MDPSLimit = 0;
  long BetFrequency = 0;
  char TickType;
  std::cout << "Please set your tick type: " << std::endl;
  std::cin >> TickType;
  std::cout << "Please set the initial amount:" << std::endl;
  std::cin >> InitialAmount;

  std::cout << "Please set your preferred frequency for calculating returns:" << std::endl;
  std::cin >> ReturnsFrequency;
  std::cin.clear();
  std::cin.ignore();
  std::cout << "Please set your limit for calculating the number of trades considered as Maximum Dollar Position Size:" << std::endl;
  std::cin >> MDPSLimit;
  std::cin.clear();
  std::cin.ignore();

  std::cout << "Please set your preferred frequency for calculating the number of bets:" << std::endl;
  std::cin >> BetFrequency;
  std::cin.clear();
  std::cin.ignore();

  static_pointer_cast<StrategyInterface>(st.second)->setInitialAmount(InitialAmount);
  static_pointer_cast<StrategyInterface>(st.second)->SetReturnsFrequency(ReturnsFrequency);
  static_pointer_cast<StrategyInterface>(st.second)->setLimitMDPS(MDPSLimit);
  static_pointer_cast<StrategyInterface>(st.second)->setBetFrequency(BetFrequency);
  static_pointer_cast<StrategyInterface>(st.second)->setTickChoice(TickType);
  static_pointer_cast<StrategyInterface>(st.second)->setDollarBarParam(100000);
  // static_pointer_cast<StrategyInterface>(st.second)->setInitialAmount(20000000);
  // static_pointer_cast<StrategyInterface>(st.second)->SetReturnsFrequency(10000000000);
  // static_pointer_cast<StrategyInterface>(st.second)->setLimitMDPS(10);
  // static_pointer_cast<StrategyInterface>(st.second)->setBetFrequency(100000000000);
  std::cout
      << "Backtester" << std::endl;
  std::cout << "Strategy:         " << st.first << std::endl;
  std::cout << "Source:           " << input << std::endl;
  std::cout << "Recording Trades: " << csv_output << std::endl;
  std::cout << "Recording Orders: " << db_output << std::endl;

  std::ifstream file(input);
  CsvLoader dl = CsvLoader(file);
  dl.registerListener(st.second);
  dl.registerTimeManager(tm);
  std::thread t1([&]() -> void
                 { bk.run(); }),
      t2([&](auto k) -> void
         { k.run(); },
         dl);
  t1.join();
  t2.join();
  static_pointer_cast<StrategyInterface>(st.second)->getReturns();
  // int size = static_pointer_cast<StrategyInterface>(st.second)->Returns.size();

  std::cout << "____________" << std::endl;
  // for (int i = 0; i < size; i++)
  // {
  //   std::cout << static_pointer_cast<StrategyInterface>(st.second)->Returns[i] << "  ";
  // }

  // for (auto const &x : static_pointer_cast<StrategyInterface>(st.second)->AssetPrice)
  // {

  //   std::cout << x.second << " ";
  // }
  std::cout << std::endl;
  std::cout << "BACKTEST STATISTICS" << std::endl;
  std::cout << "------------------" << std::endl;
  std::cout << "Net Profit: " << static_pointer_cast<StrategyInterface>(st.second)->getNetProfit() << std::endl;
  std::cout << "Initial Amount: " << static_pointer_cast<StrategyInterface>(st.second)->initialAmount << std::endl;
  std::cout << "Final Amount: " << static_pointer_cast<StrategyInterface>(st.second)->currentAmount << std::endl;

  std::cout << "Sharpe Ratio: " << static_pointer_cast<StrategyInterface>(st.second)->getSharpeRatio() << std::endl;
  std::cout << "Average AUM: " << static_pointer_cast<StrategyInterface>(st.second)->getAverageAUM() << std::endl;
  std::cout << "Leverage: " << static_pointer_cast<StrategyInterface>(st.second)->getLeverage() << std::endl;
  std::cout << "Maximum Dollas Position Size: " << static_pointer_cast<StrategyInterface>(st.second)->getMDPS() << std::endl;
  // std::cout << "Here are the number of bets per the frequency chosen: " << std::endl;
  // for (int i = 0; i < static_pointer_cast<StrategyInterface>(st.second)->getBetFrequency().size(); i++)
  // {
  //   std::cout << static_pointer_cast<StrategyInterface>(st.second)->getBetFrequency()[i] << " ";
  // }

  std::cout << std::endl;
  std::cout << "Average Holding Period: " << static_pointer_cast<StrategyInterface>(st.second)->getAverageHoldingPeriod() << std::endl;

  std::cout << "______________________" << std::endl;

  // for (int i = 0; i < static_pointer_cast<StrategyInterface>(st.second)->TickHistory.size(); i++)
  //   std::cout << static_pointer_cast<StrategyInterface>(st.second)->TickHistory[i].close << " ";
  // std::cout << "_______________________" << std::endl;
  // std::cout << "____________" << std::endl;
  // std::cout << "___________" << std::endl;
  // st->printOrders();
  // std::cout << "___________" << std::endl;
  return 0;
}
