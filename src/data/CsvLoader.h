#pragma once
#include <memory> 
#include <chrono>
#include <iostream>
#include <fstream>
#include <sstream>
#include <thread>
#include "EventListener.h"
#include "messages.h"
#include "TimeManager.h"

class CsvLoader {
    private:
        std::ifstream& _file;
        std::shared_ptr<EventListener> listener;
        std::shared_ptr<TimeManager> tmgr;
        std::shared_ptr<Event> processOne(std::string& line);
    public:
        CsvLoader(std::ifstream& file);
        void registerListener(std::shared_ptr<EventListener> listener);
        void registerTimeManager(std::shared_ptr<TimeManager> tmgr);
        void run();
    
};
