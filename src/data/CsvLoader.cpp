#include "CsvLoader.h"


CsvLoader::CsvLoader(std::ifstream & file) : _file(file) {}

void CsvLoader::registerListener(std::shared_ptr<EventListener> listener) {
    this->listener = listener;
}

void CsvLoader::registerTimeManager(std::shared_ptr<TimeManager> tmgr) {
    this->tmgr = tmgr;
}


void CsvLoader::run() {
    std::string line;
    
    if (_file.is_open()) {
        getline (_file,line);
        while ( getline (_file,line) )
        {
            auto evt = processOne(line);
            if (evt == nullptr) continue;
            listener->push(evt);
            tmgr->ProceedFetchToTime(evt->deliver_time);
        }
        _file.close();
    }
    tmgr->session_ended = true;
    tmgr->ProceedFetchToTime(__LONG_MAX__);
    std::cout << "DL complete!" << std::endl;
}

std::shared_ptr<Event> CsvLoader::processOne(std::string& line) {
    std::istringstream ss(line);
    long time;
    ss >> time;
    ss.ignore(1);
    char type;
    ss >> type;
    int id;
    ss.ignore(1);
    ss >> id;
    ss.ignore(1);
    short s;
    ss >> s;
    Side side = (Side) s;
    ss.ignore(1);
    long size;
    float price;
    ss >> size;
    ss.ignore(1);
    ss >> price;
    switch (type) {
        case 'A':
            {
            auto msgA = std::make_shared<AddMessage>();
            msgA->stockLocate = 0;
            msgA->source = SOURCE_HISTORICAL;
            msgA->deliver_time = time;
            msgA->side = side;
            msgA->orderNumber = id;
            msgA->price = price;
            msgA->quantity = size;
            return msgA;
            }
        case 'D':
            {
            ss.ignore(1);
            ss >> size;
            auto msgC = std::make_shared<CancelMessage>();
            msgC->stockLocate = 0;
            msgC->source = SOURCE_HISTORICAL;
            msgC->deliver_time = time;
            msgC->orderNumber = id;
            msgC->quantity = size;
            msgC->price = price;
            msgC->side = side;
            return msgC;
            }
        case 'E':
        case 'P':
        case 'C':
        {
          return nullptr;
        }
        case 'R':
        {
            ss.ignore(3);
            int old_id;
            ss >> old_id;
            auto msgU = std::make_shared<UpdateMessage>();
            msgU->stockLocate = 0;
            msgU->source = SOURCE_HISTORICAL;
            msgU->deliver_time = time;
            msgU->orderNumber = old_id;
            msgU->price = price;
            msgU->quantity = size;
            msgU->newOrderNumber = id;
            msgU->side = side;
            return msgU;
        }
        default:
        {
            std::cerr << "Unknown Type: " << type << std::endl;
        }
    }
    return nullptr;
    

}
