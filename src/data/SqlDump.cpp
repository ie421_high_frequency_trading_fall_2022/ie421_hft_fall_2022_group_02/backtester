#include "SqlDump.h"
#include <stdexcept>

SqlDump::SqlDump(std::string path) {
    sqlite3_open(path.c_str(), &_out);
    sqlite3_open(":memory:", &_db);
    char *err;
    int exit;
    exit = sqlite3_exec(_db, sql.c_str(), NULL, 0, &err);
    if (exit)
    {
        std::cerr << "Error open DB " << sqlite3_errmsg(_db) << std::endl;
    }
    else
        std::cout << "Opened Database Successfully!" << std::endl;
}

SqlDump::~SqlDump() {
    sqlite3_backup *pBackup = sqlite3_backup_init(_out, "main", _db, "main");
    if (pBackup)
    {
        (void)sqlite3_backup_step(pBackup, -1);
        (void)sqlite3_backup_finish(pBackup);
        int rc = sqlite3_errcode(_out);
        if (rc)
            std::cerr << "Saving to disk failed with code: " << rc << std::endl;
    }

    sqlite3_close(_db);
}

void SqlDump::setEndToOrder(int id, long end) {
    std::stringstream ss;
    ss << "UPDATE ORDERS SET END="
       << end << ' '
       << "WHERE id=" << id << " AND end=" << __LONG_MAX__ << ';';
    execute(ss);
}

void SqlDump::insertToOrder(int id, long start, long pri, int quan, float price, int side) {
    std::stringstream ss;
    ss << "INSERT INTO ORDERS VALUES ("
       << id << ','
       << start << ','
       << __LONG_MAX__ << ','
       << pri << ','
       << quan << ','
       << price << ','
       << side << ")"
       << " ON CONFLICT(id, start) DO UPDATE SET quantity=excluded.quantity, end=9223372036854775807;"
       << std::endl;
    execute(ss);
}

void SqlDump::execute(std::stringstream &ss) {
    std::string cmd = ss.str();
    char *error;
    int exit = sqlite3_exec(_db, cmd.c_str(), NULL, 0, &error);
    if (exit)
    {
        std::cerr << "Error executing:\n"
                  << cmd << "\nError: " << sqlite3_errmsg(_db) << std::endl;
        throw std::runtime_error("CONFLICT");
    }
}

long SqlDump::getPriority(int id) {
    char *err;
    long pri;
    std::stringstream ss;
    ss << "SELECT PRIORITY FROM ORDERS WHERE ID=" << id << ';';
    sqlite3_exec(
        _db, ss.str().c_str(), +[](void *a, int argc, char **argv, char **cols)
                               {
        *(long*) a = std::stol(argv[0]);
        return 0; },
        &pri, &err);
    return pri;
}

int SqlDump::getQuantity(int id)
{
    char *err;
    int res;
    std::stringstream ss;
    ss << "SELECT QUANTITY FROM ORDERS WHERE ID=" << id << " AND end=" << __LONG_MAX__ << ';';
    sqlite3_exec(
        _db, ss.str().c_str(), +[](void *a, int argc, char **argv, char **cols)
                               {
        *(int*) a = std::stol(argv[0]);
        return 0; },
        &res, &err);
    return res;
}

void SqlDump::addOrder(const std::shared_ptr<Event> &evt) {
    auto add = std::static_pointer_cast<AddMessage>(evt);
    insertToOrder(add->orderNumber, add->deliver_time, add->deliver_time, add->quantity, add->price, add->side);
}

void SqlDump::updateOrder(const std::shared_ptr<Event> &evt) {
    auto upt = std::static_pointer_cast<UpdateMessage>(evt);
    setEndToOrder(upt->orderNumber, upt->deliver_time);
    insertToOrder(upt->newOrderNumber, upt->deliver_time, upt->deliver_time, upt->quantity, upt->price, upt->side);
}

void SqlDump::cancelOrder(const std::shared_ptr<Event> &evt) {
    auto can = std::static_pointer_cast<CancelMessage>(evt);
    long pri = getPriority(can->orderNumber);
    int quan = getQuantity(can->orderNumber);
    setEndToOrder(can->orderNumber, can->deliver_time);
    if (quan - can->quantity)
    {
        insertToOrder(can->orderNumber, can->deliver_time, pri, quan - can->quantity, can->price, can->side);
    }
}

void SqlDump::changePosition(std::shared_ptr<TradeMessage> td) {
    if (td->orderNumber < 0) {
        if (td->aggressedSide == Side::B) {
            cash -= td->price * td->quantity;
            shares += td->quantity;
        } else {
            cash += td->price * td->quantity;
            shares -= td->quantity;
        }
    } else {
        if (td->aggressedSide == Side::S) {
            cash -= td->price * td->quantity;
            shares += td->quantity;
        } else {
            cash += td->price * td->quantity;
            shares -= td->quantity;
        }
    }
    refershPNL(td->deliver_time);
}

void SqlDump::refershPNL(long time) {
    std::stringstream ss;
    ss << "INSERT INTO POSITION VALUES ("
       << time << ','
       << shares <<  ','
       << cash << ','
       << shares * ref_price + cash << ");";
    execute(ss);
}

void SqlDump::tradeOrder(const std::shared_ptr<Event> &evt) {
    auto td = std::static_pointer_cast<TradeMessage>(evt);
    if (td->price != ref_price) {
        ref_price = td->price;
        refershPNL(td->deliver_time);
    }
    
    std::stringstream ss;
    ss << "INSERT INTO TRADES VALUES ("
       << (td->source == 'S' ? 1 : 0) << ','
       << td->deliver_time << ','
       << td->quantity << ','
       << td->price << ','
       << 0 << ");";
    execute(ss);
    if (td->source == 'S') {
        changePosition(td);
    }
}

void SqlDump::record(std::shared_ptr<Event> event) {
    switch (event->type)
    {
    case Type::A:
        addOrder(event);
        break;
    case Type::U:
        updateOrder(event);
        break;
    case Type::C:
        cancelOrder(event);
        break;
    case Type::T:
        tradeOrder(event);
        break;
    default:
        std::cerr << "WARN unidentified type: " << (int)event->type << std::endl;
    }
}

