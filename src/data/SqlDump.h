#pragma once
#include <sqlite3.h>
#include <string>
#include <memory>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include "Event.h"
#include "messages.h"
#include <tr1/unordered_map>
class SqlDump
{

private:
    sqlite3 *_db;
    sqlite3 *_out;
    std::string sql = "CREATE TABLE TRADES("
                      "USER          INT     NOT NULL,"
                      "TIMESTAMP      LONG    NOT NULL, "
                      "QUANTITY       INT     NOT NULL, "
                      "PRICE          REAL     NOT NULL, "
                      "AGRESSED       INT); "
                      "CREATE TABLE ORDERS("
                      "ID INT    NOT NULL, "
                      "START      LONG    NOT NULL, "
                      "END      LONG    NOT NULL, "
                      "PRIORITY LONG NOT NULL, "
                      "QUANTITY       INT     NOT NULL, "
                      "PRICE          REAL     NOT NULL, "
                      "SIDE       INT NOT NULL,"
                      "PRIMARY KEY (ID, START) );"
                      "CREATE TABLE POSITION("
                      "TIME LONG,"
                      "SHARES REAL,"
                      "CASH REAL,"
                      "PNL REAL"
                      ");";

    void addOrder(const std::shared_ptr<Event> &evt);
    void updateOrder(const std::shared_ptr<Event> &evt);
    void cancelOrder(const std::shared_ptr<Event> &evt);
    void tradeOrder(const std::shared_ptr<Event> &evt);
    void insertToOrder(int id, long start, long pri, int quan, float price, int side);
    void setEndToOrder(int id, long end);
    long getPriority(int id);
    void execute(std::stringstream &ss);
    int getQuantity(int id);
    void changePosition(std::shared_ptr<TradeMessage> td);
    void refershPNL(long time);
    float ref_price = 0;
    int shares = 0;
    int cash = 0;

public:
    SqlDump(std::string path);
    ~SqlDump();
    void record(std::shared_ptr<Event> evt);
    std::tr1::unordered_map<int, int> orders;
};